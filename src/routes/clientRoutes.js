import HomePage from "../pages/home/HomePage";
import PersonalRoom from "../pages/PersonalRoom";
import AccessoriesFiltered from "../pages/products/FilteredLists/AccessoriesFiltered";
import BagsFiltered from "../pages/products/FilteredLists/BagsFiltered";
import Cart from "../pages/products/Cart/Cart";
import Favourites from "../pages/products/SpecialLists/Favourites";
import MostSold from "../pages/products/SpecialLists/MostSold";
import NewSeason from "../pages/products/SpecialLists/NewSeason";
import ProductDetails from "../pages/products/ProductDetails";
import Products from "../pages/products/Products";
import ShoesFiltered from "../pages/products/FilteredLists/ShoesFiltered";
import Promotions from "src/pages/products/SpecialLists/Promotions";

const clientRoutes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/home", name: "Home", exact: true, component: HomePage },
  { path: "/home/:banner", name: "Home", exact: true, component: HomePage },
  { path: "/home/:banner", name: "Home", exact: true, component: HomePage },

  { path: "/products", name: "Products", component: Products, exact: true },
  {
    path: "/products/detail/:id",
    name: "Products",
    component: ProductDetails,
    exact: true,
  },
  {
    path: "/products/:category",
    name: "Products by category",
    component: Products,
  },

  { path: "/shoes/:type", name: "Shoes by type", component: ShoesFiltered },
  { path: "/bag/:type", name: "Bags by type", component: BagsFiltered },
  {
    path: "/accessories/:type",
    name: "Bags by type",
    component: AccessoriesFiltered,
  },

  { path: "/most-sold", name: "Products most sold", component: MostSold },
  { path: "/new-season", name: "Products new season", component: NewSeason },
  { path: "/promotions", name: "Products promotions", component: Promotions },
  { path: "/favourites", name: "Favourites", component: Favourites },

  { path: "/personal-room", name: "Personal room", component: PersonalRoom },
  { path: "/cart", name: "Cart", component: Cart },
];

export default clientRoutes;
