import adminRoutes from "./adminRoutes";
import clientRoutes from "./clientRoutes";

const routes = [...adminRoutes, ...clientRoutes];
export default routes;
