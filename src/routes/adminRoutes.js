import CategoryForm from "../pages/admin/category/CategoryForm";
import CategoryList from "../pages/admin/category/CategoryList";
import ColorForm from "../pages/admin/colors/ColorForm";
import Colors from "../pages/admin/colors/Colors";
import MarkForm from "../pages/admin/marks/MarkForm";
import MarkList from "../pages/admin/marks/MarkList";
import OrderList from "../pages/admin/orders/OrderList";
import ProblemForm from "../pages/admin/problems/ProblemForm";
import Problems from "../pages/admin/problems/Problems";
import ProductForm from "../pages/admin/product/ProductForm";
import ProductList from "../pages/admin/product/ProductList";
import SubCategoryForm from "../pages/admin/subcategory/SubCategoryForm";
import SubCategoryList from "../pages/admin/subcategory/SubCategoryList";
import HomePage from "../pages/home/HomePage";

const adminRoutes = [
  { path: "/admin", name: "Admin", component: HomePage, exact: true },
  { path: "/admin/categories", name: "Categories", component: CategoryList },
  {
    path: "/admin/category/create",
    name: "Categories Create",
    component: CategoryForm,
  },
  {
    path: "/admin/category/edit/:id",
    name: "Categories Edit",
    component: CategoryForm,
  },

  { path: "/admin/marks", name: "Marks", component: MarkList },
  { path: "/admin/mark/create", name: "Marks Create", component: MarkForm },
  { path: "/admin/mark/edit/:id", name: "Marks Edit", component: MarkForm },

  { path: "/admin/problems", name: "Problems", component: Problems },
  {
    path: "/admin/problem/create",
    name: "Problems Create",
    component: ProblemForm,
  },
  {
    path: "/admin/problem/edit/:id",
    name: "Problems Edit",
    component: ProblemForm,
  },

  { path: "/admin/colors", name: "Colors", component: Colors },
  { path: "/admin/color/create", name: "Colors Create", component: ColorForm },
  { path: "/admin/color/edit/:id", name: "Colors Edit", component: ColorForm },

  {
    path: "/admin/subcategories",
    name: "SubCategories",
    component: SubCategoryList,
  },
  {
    path: "/admin/subcategory/create",
    name: "Subcategory Create",
    component: SubCategoryForm,
  },
  {
    path: "/admin/subcategory/edit/:id",
    name: "Subcategory Edit",
    component: SubCategoryForm,
  },

  { path: "/admin/orders", name: "Orders", component: OrderList },

  { path: "/admin/products", name: "Products", component: ProductList },
  {
    path: "/admin/product/create",
    name: "Product Create",
    component: ProductForm,
  },
  {
    path: "/admin/product/edit/:id",
    name: "Products Edit",
    component: ProductForm,
  },
];

export default adminRoutes;
