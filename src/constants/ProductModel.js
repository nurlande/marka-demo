export const ProductModel = () => {
  return {
    name: "",
    link: "",
    image: "",
    mark: "",
    category: "",
    subCategory: "",
    price: 0,
    priceOriginal: "",
    description: "",
    colors: [],
    sizes: [],
    comments: [],
    soldCount: 0,
    isNewSeason: false,
    createdAt: new Date(),
    updatedAt: new Date(),
    season: "",
    seasons: [],
    viewType: "",
    height: "",
  };
};
