
import "firebase/firestore"; // access firestore database service
import {getFirestore} from "firebase/firestore"; 
import {getStorage} from "firebase/storage"; 
import {getAuth} from "firebase/auth"; 
import { initializeApp } from 'firebase/app';

const firebaseConfig = {
  // apiKey: "AIzaSyC2ZhoSo2DPtI9i1wH2Ts3MTAW0HlewCjM",
  // authDomain: "marka-kiy.firebaseapp.com",
  // projectId: "marka-kiy",
  // databaseURL:  "https://marka-kiy.firebaseio.com",
  // storageBucket: "marka-kiy.appspot.com",
  // messagingSenderId: "439152966092",
  // appId: "1:439152966092:web:dadb42d218199ca3ee4569",
  // measurementId: "G-WN9VLRDZQV"
  apiKey: "AIzaSyDp5s_YW85cI8wFi7ZJw2IlQefUf38wMus",
  authDomain: "chokoichum.firebaseapp.com",
  projectId: "chokoichum",
  storageBucket: "chokoichum.appspot.com",
  messagingSenderId: "55981291182",
  appId: "1:55981291182:web:b5af1d74c0f5ad1ff41f3f"
};

// Initialize Firebase

const app = initializeApp(firebaseConfig);

// firestore
export const fire = getFirestore(app)

// storage rules : request.auth != null
export const fireStorage = getStorage(app)

// auth
export const fireAuth = getAuth(app)