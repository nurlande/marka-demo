import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import ProductService from "src/services/ProductService";
import List from "./List";

export default function Products() {
  const params = useParams("category");

  const [products, setProducts] = useState([]);

  useEffect(() => {
    if (params.category) {
      const category =
        params.category === "shoes"
          ? "Shoes"
          : params.category === "bag"
          ? "Bag"
          : "Accessories";
      ProductService.getAll({ category: category }).then((rs) => {
        setProducts(rs);
      });
    }
  }, [params]);

  return (
    <>
      <List products={products} />
    </>
  );
}
