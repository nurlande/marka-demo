import { CCol, CRow } from "@coreui/react";
import React from "react";
import { useSelector } from "react-redux";
import ProductCard from "./ProductCard";
import { useHistory } from "react-router-dom";

export default function ProductGrid({ products, grid, maxSize }) {
  const history = useHistory();

  const cart = useSelector((state) => state.product.cart);

  const showDetails = (product) => {
    history.push("/products/detail/" + product?.id);
  };

  const checkOnCart = (product) => {
    return cart.filter((c) => c.id === product?.id).length > 0;
  };

  return (
    <div className="mx-2">
      <CRow>
        {products
          .filter((p, i) => (maxSize ? i < maxSize : true))
          .map((p, i) => (
            <CCol sm={grid || "3"} xs="6" key={i} className="my-1">
              <ProductCard
                pro={p}
                checkOnCart={checkOnCart}
                showDetails={showDetails}
              />
            </CCol>
          ))}
      </CRow>
    </div>
  );
}
