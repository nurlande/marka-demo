import { cilX } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CFormTextarea,
  CRow,
  CTable,
  CTableBody,
} from "@coreui/react";
import { getAuth } from "firebase/auth";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import MiniSpinner from "src/components/spinners/MiniSpinner";
import { toastify } from "src/helpers/toast";
import { addToCart, removeFromCart } from "src/redux/actions/productActions";
import ProductService from "src/services/ProductService";
import ProductGrid from "./ProductGrid";

export default function ProductDetails() {
  const { t, i18n } = useTranslation();
  const dispatch = useDispatch();
  const params = useParams();
  const cart = useSelector((state) => state.product.cart);
  const mode = useSelector((state) => state.settings.mode);

  const [selectedProduct, setSelectedProduct] = useState(null);
  const [selectedColor, setSelectedColor] = useState(null);
  const [selectedSize, setSelectedSize] = useState(null);
  const [newComment, setNewComment] = useState("");
  const [selectedImg, setSelectedImg] = useState(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (params.id) {
      setLoading(true);
      ProductService.getById(params.id).then((res) => {
        console.log(res.data());
        setSelectedProduct({ id: res.id, ...res.data() });
        setLoading(false);
      });
    }
  }, [params]);

  const checkOnCart = (product) => {
    return cart.filter((c) => c.id === product?.id).length > 0;
  };

  const updateComments = (newComments) => {
    ProductService.updateComments(selectedProduct?.id, newComments)
      .then((res) => {
        setSelectedProduct({
          ...selectedProduct,
          comments: newComments,
        });
        setNewComment("");
        toastify("success", "Operation successful");
      })
      .catch((err) => {
        toastify("error", "Operation failed");
        console.log(err);
      });
  };

  const submitComment = () => {
    const oldComments = selectedProduct.comments || [];
    updateComments([...oldComments, { text: newComment }]);
  };

  const deleteComment = (index) => {
    const oldComments = selectedProduct.comments || [];
    oldComments.splice(index, 1);
    updateComments([...oldComments]);
  };

  return (
    <CCard>
      {loading ? (
        <div className="mt-5 text-center">
          <MiniSpinner />
        </div>
      ) : (
        <CCardBody>
          {selectedProduct && (
            <CRow>
              <CCol sm="5">
                <img
                  className="img w-100 img-responsive mb-1"
                  alt=""
                  src={
                    selectedImg ||
                    (selectedProduct.images
                      ? selectedProduct.images[0]
                      : selectedProduct.image) ||
                    "https://martialartsplusinc.com/wp-content/uploads/2017/04/default-image-620x600.jpg"
                  }
                />
                <CRow>
                  {selectedProduct.images?.map((img, i) => (
                    <CCol sm="4" xs="2" key={i}>
                      <img
                        className="m-1"
                        alt="proimg"
                        src={img}
                        style={{ maxWidth: "100%" }}
                        onClick={() => setSelectedImg(img)}
                      />
                    </CCol>
                  ))}
                </CRow>
                <div className="d-flex justify-content-end mb-1 d-sm-block d-md-none">
                  <CButton
                    size="sm"
                    variant="outline"
                    onClick={() =>
                      navigator
                        .share({
                          title:
                            i18n.language === "ky"
                              ? selectedProduct.name
                              : selectedProduct.name_ru || selectedProduct.name,
                          url:
                            "https://marka-kiy.web.app/products/detail/" +
                            selectedProduct.id,
                        })
                        .then((res) => console.log("shared", res))
                        .catch((err) => console.log("error", err))
                    }
                  >
                    {t("Share")}
                  </CButton>
                </div>
              </CCol>
              <CCol sm="7">
                <div className="d-flex justify-content-end">
                  <CButton
                    className={
                      "mx-1 " +
                      (checkOnCart(selectedProduct)
                        ? "btn-danger"
                        : "btn-warning")
                    }
                    onClick={() =>
                      checkOnCart(selectedProduct)
                        ? dispatch(removeFromCart(selectedProduct))
                        : dispatch(
                            addToCart({
                              ...selectedProduct,
                              selectedColor,
                              selectedSize,
                            })
                          )
                    }
                  >
                    {checkOnCart(selectedProduct)
                      ? t("Remove from cart")
                      : t("Add to cart")}
                  </CButton>
                </div>
                <h2>
                  {i18n.language === "ky"
                    ? selectedProduct.name
                    : selectedProduct.name_ru || selectedProduct.name}
                </h2>
                <div className="table-responsive-sm">
                  <CTable className="table">
                    <CTableBody>
                      <tr>
                        <td>{t("Mark")}</td>
                        <td>
                          {selectedProduct.mark?.icon ? (
                            <img
                              src={selectedProduct.mark?.icon}
                              width="100"
                              alt="mark"
                            />
                          ) : (
                            selectedProduct?.mark?.name || selectedProduct?.mark
                          )}
                        </td>
                      </tr>
                      <tr>
                        <td>{t("Colors")}</td>
                        <td>
                          {(i18n.language === "ky"
                            ? selectedProduct.colors
                            : selectedProduct.colors_ru ||
                              selectedProduct.colors
                          )?.map((c, i) => (
                            <CButton
                              size="sm"
                              color="secondary"
                              className="mx-1"
                              key={i}
                              onClick={() => setSelectedColor(c)}
                              variant={selectedColor !== c && "outline"}
                            >
                              {c}
                            </CButton>
                          ))}
                        </td>
                      </tr>
                      <tr>
                        <td>{t("Sizes")}</td>
                        <td>
                          {selectedProduct.sizes?.map((c, i) => (
                            <CButton
                              size="sm"
                              color="secondary"
                              className="mx-1"
                              key={i}
                              onClick={() => setSelectedSize(c)}
                              variant={selectedSize !== c && "outline"}
                            >
                              {c}
                            </CButton>
                          ))}
                        </td>
                      </tr>
                      <tr>
                        <td>{t("Season")}</td>
                        <td>
                          {selectedProduct.seasons?.map((s, i) => (
                            <CButton
                              size="sm"
                              color="secondary"
                              className="mx-1"
                              key={i}
                            >
                              {t(s)}
                            </CButton>
                          ))}
                        </td>
                      </tr>
                      <tr>
                        <td>{t("Category")}</td>
                        <td>{t(selectedProduct.viewType)}</td>
                      </tr>
                      <tr>
                        <td>{t("Price")}</td>
                        <td>{selectedProduct.price} KGS</td>
                      </tr>
                    </CTableBody>
                  </CTable>
                </div>
                <div
                  style={{ fontFamily: "'PT Mono', monospace !important" }}
                  className="mt-3"
                  dangerouslySetInnerHTML={{
                    __html:
                      i18n.language === "ky"
                        ? selectedProduct?.description
                        : selectedProduct?.description_ru ||
                          selectedProduct?.description
                            .replace("Өндүрүш", t("Production"))
                            .replace("Бийиктиги", t("Height")),
                  }}
                />
                <hr />
                <p>
                  <b>{t("Feedbacks")}:</b> <hr />
                  {selectedProduct.comments?.map((c, i) => (
                    <i key={i}>
                      {mode === "admin" && (
                        <CIcon
                          icon={cilX}
                          className="float-right mr-2"
                          onClick={() => deleteComment(i)}
                        />
                      )}
                      {c.text}
                      <hr />
                    </i>
                  ))}
                  {getAuth()?.currentUser && (
                    <>
                      <CFormTextarea
                        placeholder="өз пикириңизди калтырыңыз"
                        value={newComment}
                        onChange={(e) => setNewComment(e.target.value)}
                      />
                      <div className="d-flex justify-content-end mt-1">
                        <CButton size="sm" onClick={submitComment}>
                          {t("Submit")}
                        </CButton>
                      </div>
                    </>
                  )}
                </p>
              </CCol>
            </CRow>
          )}
          <h6 className="mt-4">{t("Related products")}</h6>
          <hr />
          <ProductGrid
            categoryFilter={"all"}
            products={[selectedProduct]}
            grid={3}
          />
        </CCardBody>
      )}
    </CCard>
  );
}
