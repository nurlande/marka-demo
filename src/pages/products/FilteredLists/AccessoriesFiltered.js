import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { useSelector } from "react-redux";
import List from "../List";

export default function AccessoriesFiltered() {
  const params = useParams("category");

  const [typeFilter, setTypeFilter] = useState(null);

  const products = useSelector((state) => state.product.products);

  useEffect(() => {
    if (params.type) {
      setTypeFilter(params.type);
    }
  }, [params]);

  return (
    <>
      {typeFilter}
      <List
        products={products.filter((p) =>
          typeFilter ? p.viewType === typeFilter : true
        )}
      />
    </>
  );
}
