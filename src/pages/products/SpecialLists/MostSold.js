import React from "react";
import { useSelector } from "react-redux";
import List from "../List";

export default function MostSold() {
  const products = useSelector((state) => state.product.products);

  return (
    <>
      <List products={products} />
    </>
  );
}
