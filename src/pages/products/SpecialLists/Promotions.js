import React from "react";
import { useSelector } from "react-redux";
import List from "../List";

export default function Promotions() {
  const products = useSelector((state) => state.product.products);

  return (
    <>
      <List products={products.filter((p) => p.isPromoted)} />
    </>
  );
}
