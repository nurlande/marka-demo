import React from "react"
import { useSelector } from 'react-redux';
import List from "../List";

export default function Favourites() {

    const products = useSelector(state => state.product.products)

    return (
        <>
            <h5>My Favourites</h5>
            <List products={products}/>
        </>
    )
}