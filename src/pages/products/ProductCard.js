import React from "react";
import { CCard, CCardBody, CCardImage, CRow, CCol } from "@coreui/react";
import { useTranslation } from "react-i18next";

export default function ProductCard(props) {
  const { i18n } = useTranslation();
  return props?.pro ? (
    <CCard
      className="my-1 font-xs procard"
      style={{ cursor: "pointer" }}
      onClick={() => props.showDetails(props.pro)}
    >
      <CCardImage
        orientation="top"
        style={{ objectFit: "cover", height: "320px" }}
        src={
          props.pro.image ||
          (props.pro.images ? props.pro.images[0] : null) ||
          "https://martialartsplusinc.com/wp-content/uploads/2017/04/default-image-620x600.jpg"
        }
      />
      <CCardBody>
        <CRow>
          <CCol sm="8">
            <div className="font-sm text-truncate">
              {i18n.language === "ky" ? props.pro.name : props.pro.name_ru || props.pro.name}
            </div>
          </CCol>
          <CCol sm="4" className="d-flex justify-content-end">
            <b className="text-bold text-success">
              {props.pro.price} <u>c</u>
            </b>
          </CCol>
        </CRow>
      </CCardBody>
    </CCard>
  ) : (
    <h5>Loading...</h5>
  );
}
