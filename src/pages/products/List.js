import { CButton, CCol, CContainer, CRow } from "@coreui/react";
import React, { useState } from "react";
import ProductGrid from "./ProductGrid";
import SearchContainer from "../common/SearchContainer";
import { useTranslation } from "react-i18next";
import FilterContainer from "../common/FilterContainer";

export default function List({ products }) {
  const { t } = useTranslation();

  const [searchKey, setSearchKey] = useState("");

  const [showFilter, setShowFilter] = useState(false);
  const [priceRange, setPriceRange] = useState({ max: 100000, min: 0 });

  return (
    <>
      <CContainer className="mt-2">
        <CRow>
          <CCol sm="10">
            <SearchContainer onChange={setSearchKey} value={searchKey} />
          </CCol>
          <CCol sm="2">
            <CButton onClick={() => setShowFilter(true)} variant="outline">
              {t("Filter")}
            </CButton>
          </CCol>
        </CRow>
      </CContainer>
      <ProductGrid
        products={(searchKey
          ? products.filter(
              (p) =>
                p.name.toUpperCase().includes(searchKey.trim().toUpperCase()) ||
                p.name_ru
                  ?.toUpperCase()
                  ?.includes(searchKey.trim().toUpperCase()) ||
                p.mark?.name
                  ?.toUpperCase()
                  .includes(searchKey.trim().toUpperCase()) ||
                p.description
                  .toUpperCase()
                  .includes(searchKey.trim().toUpperCase())
            )
          : products
        ).filter((p) => priceRange.min <= p.price && priceRange.max >= p.price)}
      />
      <FilterContainer
        onClose={() => setShowFilter(false)}
        show={showFilter}
        range={priceRange}
        setRange={setPriceRange}
      />
    </>
  );
}
