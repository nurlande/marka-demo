import {
  CButton,
  CForm,
  CFormInput,
  CFormLabel,
  CFormTextarea,
  CModal,
  CModalBody,
  CModalHeader,
} from "@coreui/react";
import { useState } from "react";
import { useTranslation } from "react-i18next";

export default function OrderForm({
  show,
  onClose,
  fetchOrderCreate,
  response,
  loading,
}) {
  const { t } = useTranslation();
  const [order, setOrder] = useState({
    receiverName: "",
    receiverPhone: "",
    description: "",
    status: "created",
  });

  const handleChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    setOrder({ ...order, [name]: value });
  };

  const [validations, setValidations] = useState({
    receiverName: null,
    receiverPhone: null,
  });

  const submitOrder = (payload) => {
    if (order.receiverName && order.receiverPhone) {
      fetchOrderCreate(payload);
    } else {
      setValidations({
        receiverName: order.receiverName ? null : "Алуучунан атын жазыныз",
        receiverPhone: order.receiverPhone ? null : "Телефон номерин жазыныз",
      });
    }
  };

  return (
    <CModal visible={show} onClose={onClose}>
      <CModalHeader>{t("Order Form")}</CModalHeader>
      <CModalBody>
        {response ? (
          <div>
            <div>{response}</div>
            <CButton size="sm" color="info" onClick={onClose}>
              Закрыть
            </CButton>
          </div>
        ) : (
          <CForm>
            <CFormLabel>{t("Receiver")}</CFormLabel>
            <CFormInput
              value={order.receiverName}
              onChange={handleChange}
              type="text"
              placeholder="name"
              name="receiverName"
            />
            {validations.receiverName && (
              <div style={{ fontSize: "12px" }} className="text-danger">
                * {validations.receiverName}
              </div>
            )}

            <CFormLabel>{t("Phone")}</CFormLabel>
            <CFormInput
              value={order.receiverPhone}
              onChange={handleChange}
              type="text"
              placeholder="phone"
              name="receiverPhone"
            />
            {validations.receiverPhone && (
              <div style={{ fontSize: "12px" }} className="text-danger">
                * {validations.receiverPhone}
              </div>
            )}

            <CFormLabel>{t("Description")}</CFormLabel>
            <CFormTextarea
              value={order.description}
              onChange={(e) => handleChange(e)}
              name="description"
              placeholder="Description..."
            ></CFormTextarea>

            <div className="d-flex flex-row-reverse mt-2">
              <CButton
                className="mx-2"
                size="sm"
                color="danger"
                onClick={onClose}
              >
                {t("Cancel")}
              </CButton>
              <CButton
                className="mx-2"
                size="sm"
                color="success"
                onClick={submitOrder}
                disabled={loading}
              >
                {loading ? "Creating" : "Create"}
              </CButton>
            </div>
          </CForm>
        )}
      </CModalBody>
    </CModal>
  );
}
