import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CFormInput,
  CTable,
  CTableBody,
  CTableHead,
} from "@coreui/react";
import { addDoc, collection, setDoc, doc } from "@firebase/firestore";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { fire } from "src/configs/fire";
import {
  emptyCart,
  removeFromCart,
  updateAmountCart,
} from "src/redux/actions/productActions";
import OrderForm from "./OrderForm";

export default function Cart() {
  const { t, i18n } = useTranslation();

  const cart = useSelector((state) => state.product.cart);

  const dispatch = useDispatch();

  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState();
  const [response, setResponse] = useState();

  const fetchOrderCreate = (order) => {
    let payload = { ...order };
    const collectionRef = collection(fire, "orders");
    payload.products = cart;
    payload.totalPrice = cart.reduce(
      (partial_sum, a) =>
        partial_sum + parseFloat(a.price) * parseFloat(a.amount),
      0
    );
    addDoc(collectionRef, payload)
      .then((res) => {
        payload.products.forEach((p) => {
          setDoc(
            doc(fire, "products", p.id),
            { soldCount: p.soldCount + p.amount },
            { merge: true }
          ).then((res) => console.log(res));
        });
        setResponse(
          "Order request accepted. We will connect you for order confirmation."
        );
        setLoading(false);
      })
      .catch((err) => {
        setResponse("Server Error");
        setLoading(false);
        console.log(err);
      });
  };

  const onClose = () => {
    setResponse(null);
    setShow(false);
  };

  return (
    <CCard className="my-2 mx-2">
      <CCardHeader>{t("Your cart")}</CCardHeader>
      <CCardBody>
        {cart.length > 0 ? (
          <>
            <div className="table-responsive-sm">
              <CTable>
                <CTableHead>
                  <tr>
                    <th>{t("Name")}</th>
                    <th>{t("Mark")}</th>
                    <th>{t("Size")}</th>
                    <th>{t("Price")}</th>
                    <th style={{ width: "10%" }}>{t("Amount")}</th>
                    <th style={{ width: "10%" }}>{t("Remove")}</th>
                  </tr>
                </CTableHead>
                <CTableBody>
                  {cart.map((c, i) => (
                    <tr key={i}>
                      <td>{i18n.language === 'ky' ? c.name : c.name_ru || c.name}</td>
                      <td>{c.mark?.name}</td>
                      <td>{c.selectedSize}</td>
                      <td>{c.price}</td>
                      <td width="20">
                        <CFormInput
                          type="number"
                          value={c.amount}
                          onChange={(e) =>
                            dispatch(
                              updateAmountCart(
                                c.id,
                                parseInt(e.target.value) >= 0
                                  ? parseInt(e.target.value)
                                  : 0
                              )
                            )
                          }
                        />
                      </td>
                      <td>
                        <CButton
                          color="danger"
                          size="sm"
                          onClick={() => dispatch(removeFromCart(c))}
                        >
                          {t("Remove")}
                        </CButton>
                      </td>
                    </tr>
                  ))}
                </CTableBody>
              </CTable>
            </div>
            <div className="d-flex flex-row-reverse">
              {t("Overall")}:{" "}
              {cart.reduce(
                (partial_sum, a) =>
                  partial_sum + parseFloat(a.price) * parseFloat(a.amount),
                0
              )}{" "}
              KGS
            </div>
            <div className="d-flex flex-row-reverse">
              <CButton
                className="mx-2"
                size="sm"
                color="success"
                onClick={() => setShow(true)}
              >
                {t("Create Order")}
              </CButton>
              <CButton
                className="mx-2"
                size="sm"
                color="danger"
                onClick={() => dispatch(emptyCart())}
              >
                {t("Empty Cart")}
              </CButton>
            </div>
          </>
        ) : (
          t("Your cart is empty")
        )}
      </CCardBody>

      <OrderForm
        show={show}
        onClose={onClose}
        fetchOrderCreate={fetchOrderCreate}
        response={response}
        loading={loading}
      />
    </CCard>
  );
}
