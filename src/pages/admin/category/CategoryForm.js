import { CAlert, CButton, CCard, CCardBody, 
    CCardFooter, CCardHeader, CForm, 
    CFormInput, CFormLabel, 
    CFormTextarea } from '@coreui/react'
import React, { useEffect, useState } from 'react'
import { collection, addDoc, doc, setDoc, getDoc } from "firebase/firestore";
import { fire} from "src/configs/fire";
import { useHistory, useParams } from 'react-router';
import { toastify } from 'src/helpers/toast';
import { useTranslation } from 'react-i18next';

export default function CategoryForm() {

    const {t} = useTranslation()

    const params = useParams()
    const history = useHistory()

    const [category, setCategory] = useState({
        name: "",
        translate_kg: "",
        translate_ru: "",
        description: ""
    })

    const [loading, setLoading] = useState(false)
    const [response, setResponse] = useState(null)
    
    const [docRef, setDocRef] = useState(null)

    useEffect(() => {
        if(params.id) {
            const dRef = doc(fire, "category", params.id)
            setDocRef(dRef)
            getDoc(dRef).then(res => setCategory(res.data()))
        }
    }, [params])

    const handleChange = (e) => {
        let name = e.target.name
        let value = e.target.value
        setCategory({...category, [name]: value})
    }

    const submitForm = (e) => {
        e.preventDefault()
        setLoading(true)
        let payload = category
        console.log(payload)
        const collectionRef = collection(fire, "category");
        (params.id ? setDoc(docRef, payload) : addDoc(collectionRef, payload)).then(res => {
            setResponse("success")
            setLoading(false)
            res?.id && setCategory({})
            history.push("/admin/categories")
            toastify("success", "Operation successful")
            // add redirect
        }).catch(err => {
            toastify("error", "Operation failed")
            setResponse("failed")
            setLoading(false)
            console.log(err)
        })
    }

    const [showFields, setShowFields] = useState(false)

    console.log(category)
    return (
        <CCard className="my-2 w-75 mx-auto">
            <CCardHeader>
                {t("Category")} - {t(params.id ? "Edit" : "Create")}
            </CCardHeader>
            <CCardBody>
                
                {response && <CAlert color={response === "success" ? "success" : "danger"}>{response}</CAlert>}
                {loading ? 
                    <div>Loading</div> : 
                    <CForm>
                        <CFormLabel>{t("Name")}</CFormLabel>
                        <CFormInput value={category.name} onChange={handleChange} type="text" placeholder="Name" name="name"/>

                        <div>
                            <CButton size="sm" color="light" onClick={() => setShowFields(!showFields)}>
                                {t("Translations")}
                            </CButton>
                        </div>

                        {showFields ? 
                        <>
                            <CFormLabel>{t("Name") + " (кыргызча)"}</CFormLabel>
                            <CFormInput value={category.translate_kg} onChange={handleChange} type="text" placeholder="Name" name="translate_kg"/>
                            
                            <CFormLabel>{t("Name") + " (на русском)"}</CFormLabel>
                            <CFormInput value={category.translate_ru} onChange={handleChange} type="text" placeholder="Name" name="translate_ru"/>
                            
                        </> : ""}
                        <CFormLabel>{t("Description")}</CFormLabel>
                        <CFormTextarea value={category.description} onChange={e => handleChange(e)} name="description" placeholder="Description...">
                        </CFormTextarea>
                    </CForm>}
            </CCardBody>
            <CCardFooter className="d-flex flex-row-reverse">
            <CButton variant="outline" color="success" className="mx-1" 
                        disabled={loading} onClick={submitForm}>{t("Submit" + (loading ? "ing..." : ""))}</CButton>
                    <CButton variant="outline" color="secondary" className="mx-1"
                        disabled={loading} onClick={() => history.push("/admin/categories")}>{t("Back")}</CButton>
            </CCardFooter>
        </CCard>
    )
}