import { CAlert, CButton, CCard, CCardBody, 
    CCardFooter, CCardHeader, CForm, 
    CFormInput, CFormLabel, CFormSelect, 
    CFormTextarea } from '@coreui/react'
import React, { useEffect, useState } from 'react'
import { collection, addDoc, doc, setDoc, getDoc, onSnapshot, query } from "firebase/firestore";
import { fire } from "src/configs/fire";
import { useHistory, useParams } from 'react-router';
import { toastify } from 'src/helpers/toast';

export default function SubCategoryForm() {

    const params = useParams()
    const history = useHistory()

    const [subCategory, setSubCategory] = useState({
        name: "",
        description: "",
        category: undefined
    })
    const [categories, setCategories] = useState([])

    const [loading, setLoading] = useState(false)
    const [response, setResponse] = useState(null)
    
    const [docRef, setDocRef] = useState(null)

    useEffect(() => {
        const q = query(collection(fire, "category"))
        onSnapshot(q, (querySnapshot) => {
            setCategories(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
        })

        if(params.id) {
            const dRef = doc(fire, "subcategory", params.id)
            setDocRef(dRef)
            getDoc(dRef).then(res => setSubCategory(res.data()))
        }
    }, [params])

    const handleChange = (e) => {
        let name = e.target.name
        let value = e.target.value
        setSubCategory({...subCategory, [name]: value})
    } 

    const submitForm = (e) => {
        e.preventDefault()
        setLoading(true)
        let payload = subCategory
        console.log(payload)
        const collectionRef = collection(fire, "subcategory");
        (params.id ? setDoc(docRef, payload) : addDoc(collectionRef, payload)).then(res => {
            setResponse("success")
            setLoading(false)
            res?.id && setSubCategory({})
            history.push("/admin/subcategories")
            toastify("success", "Operation successful")
            // add redirect
        }).catch(err => {
            toastify("error", "Operation failed")
            setResponse("failed")
            setLoading(false)
            console.log(err)
        })
    }

    console.log(subCategory)
    return (
        <CCard className="my-2 w-75 mx-auto">
            <CCardHeader>
            Subcategory Form - {params.id ? "Edit" : "Create"}
            </CCardHeader>
            <CCardBody>
                
                {response && <CAlert color={response === "success" ? "success" : "danger"}>{response}</CAlert>}
                {loading ? 
                    <div>Loading</div> : 
                    <CForm>
                        <CFormLabel>Name</CFormLabel>
                        <CFormInput value={subCategory.name} onChange={handleChange} type="text" placeholder="Name" name="name"/>
                        
                        <CFormLabel>Category</CFormLabel>
                        <CFormSelect value={subCategory.category} onChange={e => handleChange(e)} name="category">
                            <option>None</option>
                            {categories.map((c, i) => 
                                <option value={c.name}>{c.name}</option>
                            )}
                        </CFormSelect>

                        <CFormLabel>Description</CFormLabel>
                        <CFormTextarea value={subCategory.description} onChange={e => handleChange(e)} name="description" placeholder="Description...">
                        </CFormTextarea>
                    </CForm>}
            </CCardBody>
            <CCardFooter className="d-flex flex-row-reverse">
            <CButton variant="outline" color="success" className="mx-1" 
                        disabled={loading} onClick={submitForm}>{"Submit" + (loading ? "ing..." : "")}</CButton>
                    <CButton variant="outline" color="secondary" className="mx-1"
                        disabled={loading} onClick={() => history.push("/admin/subcategories")}>Back</CButton>
            </CCardFooter>
        </CCard>
    )
}