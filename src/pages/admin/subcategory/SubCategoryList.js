import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCardTitle, CCol, CFormLabel, CFormSelect, CModal, CModalBody, CModalFooter, CModalHeader, CRow, CTable, CTableBody, CTableHead } from '@coreui/react'
import React, { useEffect, useState } from "react"
import { fire } from "src/configs/fire";
import { doc, onSnapshot, collection, query, deleteDoc} from "firebase/firestore";
import { useHistory } from 'react-router';

export default function SubCategoryList() {
    const [subCategories, setSubCategories] = useState([])
    const [categories, setCategories] = useState([])
    const [categoryFilter, setCategoryFilter] = useState(null)

    const [confirmModal, setConfirmModal] = useState(false)
    const [selectedItem, setSelectedItem] = useState(null)

    const history = useHistory()

    useEffect(() => {
        const qc = query(collection(fire, "subcategory"))
        onSnapshot(qc, (querySnapshot) => {
            setSubCategories(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
        })
        const qs = query(collection(fire, "category"))
        onSnapshot(qs, (querySnapshot) => {
            setCategories(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
        })
    }, [])

    const deleteModal = (category) => {
        setConfirmModal(true)
        setSelectedItem(category)
    }

    const confirmDelete = () => {
        deleteDoc(doc(fire, "subcategory", selectedItem.id)).then(res => {
            console.log("deleted", res)
            setConfirmModal(false)
        }).catch(err => console.log(err))
    }

    return (
        <CCard className="my-2 mx-auto">
            <CCardHeader>
                <CRow>
                    <CCol>
                        <CCardTitle>
                            Subcategories
                        </CCardTitle>
                    </CCol>
                    <CCol className="d-flex flex-row-reverse">
                        <CButton className="float-right" color="success" onClick={() => history.push("/admin/subcategory/create")}>
                            Create New
                        </CButton>
                    </CCol>
                </CRow>
            </CCardHeader>
            <CCardBody>
                <CRow>
                    <CCol sm="1">
                        <CFormLabel>Category: </CFormLabel>
                    </CCol>
                    <CCol sm="2">
                        <CFormSelect onChange={e => setCategoryFilter(e.target.value)} size="sm" value={categoryFilter}>
                            <option value="all">All</option>
                            {categories.map((c,i) => 
                                <option key={i} value={c.name}>{c.name}</option>
                            )}
                        </CFormSelect>
                    </CCol>
                </CRow>
                <div className="table-responsive mt-2">
                <CTable className="table w-100">
                    <CTableHead>
                        <tr>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                    </CTableHead>
                    <CTableBody>
                        {((categoryFilter && categoryFilter !== "all") ? 
                            subCategories.filter(su => su.category === categoryFilter) : subCategories)
                        .map((sub, i) => 
                            <tr key={i}>
                                <td>{sub.name}</td>
                                <td>{sub.category}</td>
                                <td>{sub.description ? sub.description.substring(0, 10) + "..." : sub.description}</td>
                                <td>
                                    <CButton onClick={() => history.push("/admin/subcategory/edit/" + sub.id)} className="mx-1 btn-sm"
                                        variant="outline" color="warning">Edit</CButton>
                                    <CButton onClick={() => deleteModal(sub)} className="mx-1 btn-sm"
                                        color="danger" variant="outline">Delete</CButton>
                                    {/* <CButton onClick={() => history.push("/admin/subcategory/details/" + sub.id)} className="mx-1 btn-sm"
                                        color="info" variant="outline">Details</CButton> */}
                                </td>
                            </tr>
                            )}
                    </CTableBody>
                </CTable>
                <CModal visible={confirmModal} onClose={() => setConfirmModal(false)}>
                    <CModalHeader>Confirm delete</CModalHeader>
                    <CModalBody>
                        Are you sure to delete {selectedItem?.name} ?
                    </CModalBody>
                    <CModalFooter>
                        <CButton onClick={confirmDelete}>Confirm</CButton>
                    </CModalFooter>
                </CModal>
                </div>
            </CCardBody>
            <CCardFooter>

            </CCardFooter>
        </CCard>
    )
}