import { CCol, CFormLabel, CRow } from "@coreui/react";
import React from "react";
import { useTranslation } from "react-i18next";
import SunEditor from "suneditor-react";
import "suneditor/dist/css/suneditor.min.css"; // Import Sun Editor's CSS File

export default function DescriptionTab({ product, setProduct }) {
  const { t } = useTranslation();

  return (
    <div>
      <CRow>
        <CCol sm="6">
          <CFormLabel className="mt-2">{t("Description")} (KG)</CFormLabel>
          <div>
            <SunEditor
              defaultValue={product.description}
              value={product.description}
              setContents={product.description}
              setOptions={{
                height: 200,
                buttonList: [
                  ["bold", "underline", "italic", "strike"],
                  ["removeFormat"],
                  ["outdent", "indent"],
                ],
              }}
              onChange={(rs) =>
                setProduct((prod) => {
                  return { ...prod, description: rs };
                })
              }
            />
          </div>
        </CCol>
        <CCol sm="6">
          <CFormLabel className="mt-2">{t("Description")} (RU)</CFormLabel>
          <div>
            <SunEditor
              defaultValue={product.description_ru}
              value={product.description_ru}
              setContents={product.description_ru}
              setOptions={{
                height: 200,
                buttonList: [
                  ["bold", "underline", "italic", "strike"],
                  ["removeFormat"],
                  ["outdent", "indent"],
                ],
              }}
              onChange={(rs) =>
                setProduct((prod) => {
                  return { ...prod, description_ru: rs };
                })
              }
            />
          </div>
        </CCol>
      </CRow>
    </div>
  );
}
