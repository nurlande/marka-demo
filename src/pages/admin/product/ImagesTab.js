import { CButton, CCol, CFormInput, CFormLabel, CRow } from "@coreui/react";
import React, { useState } from "react";
import { fireStorage } from "src/configs/fire";
import { getDownloadURL, ref, uploadBytesResumable } from "@firebase/storage";
import { toastify } from "src/helpers/toast";
import { useTranslation } from "react-i18next";
import { compressAccurately } from "image-conversion";

export default function ImagesTab({
  setImageUploading,
  setError,
  setProduct,
  product,
}) {
  const { t } = useTranslation();

  const [progress, setProgress] = useState(null);

  const uploadImage = (e) => {
    setImageUploading(true);
    const images = e.target.files;
    const imgs = [...images];
    let proms = [];
    imgs.forEach((img, i) => {
      if (img?.size > 1000000) {
        toastify("info", "File size is too big");
        compressAccurately(img, 200).then((res) => {
          //The res in the promise is a compressed Blob type (which can be treated as a File type) file;
          let newImg = new File([res], img.name);
          fetchImage(newImg, proms);
        });
      } else {
        fetchImage(img, proms);
      }
    });
    Promise.all(proms)
      .then((res) => setImageUploading(false))
      .catch((err) => console.log("err", err));
  };

  const fetchImage = (img, proms) => {
    const storageRef = ref(fireStorage, `images/${img.name}`);
    const uploadTask = uploadBytesResumable(storageRef, img);
    proms.push(uploadTask);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const progress = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setProgress(progress);
      },
      (error) => {
        console.log(error);
        setError(error);
      },
      async () => {
        await getDownloadURL(uploadTask.snapshot.ref).then((url) => {
          setProduct((pro) => {
            return {
              ...pro,
              images: pro.images ? [...pro.images, url] : [url],
            };
          });
        });
      }
    );
  };

  const deleteImage = (ind) => {
    setProduct({
      ...product,
      images: [...product.images.filter((img, i) => ind !== i)],
    });
  };

  return (
    <div>
      <CFormLabel className="mt-2">{t("Image upload")}</CFormLabel>
      <CFormInput
        onChange={(e) => uploadImage(e)}
        type="file"
        name="file"
        multiple
      />
      {progress ? progress + " %" : ""}
      <CRow className="mt-2">
        {product.images?.map((image, i) => (
          <CCol sm="2" className="text-center my-1" key={i}>
            <img
              alt="proimg"
              src={image}
              style={{ maxWidth: "100%", marginBottom: "3px" }}
            />
            <CButton
              size="sm"
              variant="outline"
              color="danger"
              onClick={() => deleteImage(i)}
            >
              {t("Delete")}
            </CButton>
          </CCol>
        ))}
      </CRow>
    </div>
  );
}
