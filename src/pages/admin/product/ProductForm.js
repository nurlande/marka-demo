import {
  CAlert,
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CForm,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
} from "@coreui/react";
import React, { useEffect, useState } from "react";
import { collection, getDocs, query } from "firebase/firestore";
import { fire } from "src/configs/fire";
import { ProductModel } from "src/constants/ProductModel";
import { useHistory, useParams } from "react-router";
import { toastify } from "src/helpers/toast";
import { useTranslation } from "react-i18next";
import ProductService from "src/services/ProductService";
import ImagesTab from "./ImagesTab";
import MainTab from "./MainTab";
import DescriptionTab from "./DescriptionTab";

export default function ProductForm() {
  const { t } = useTranslation();
  const params = useParams();
  const history = useHistory();

  const [product, setProduct] = useState(ProductModel());

  const [categories, setCategories] = useState([]);
  const [marks, setMarks] = useState([]);
  const [colors, setColors] = useState([]);

  const [loading, setLoading] = useState(false);
  const [imageUploading, setImageUploading] = useState(false);
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);

  const [activeKey, setActiveKey] = useState(1);

  useEffect(() => {
    const q = query(collection(fire, "category"));
    getDocs(q).then((querySnapshot) => {
      setCategories(
        querySnapshot.docs.map((d) => {
          return { ...d.data(), id: d.id };
        })
      );
    });
    const qm = query(collection(fire, "mark"));
    getDocs(qm).then((querySnapshot) => {
      setMarks(
        querySnapshot.docs.map((d) => {
          return { ...d.data(), id: d.id };
        })
      );
    });
    const qc = query(collection(fire, "colors"));
    getDocs(qc).then((querySnapshot) => {
      setColors(
        querySnapshot.docs.map((d) => {
          return { ...d.data(), id: d.id };
        })
      );
    });
  }, []);

  useEffect(() => {
    if (params.id) {
      ProductService.getById(params.id).then((res) => setProduct(res.data()));
    }
  }, [params]);

  const submitForm = (e) => {
    e.preventDefault();
    setLoading(true);
    (params.id
      ? ProductService.update(params.id, { ...product, updatedAt: new Date() })
      : ProductService.create(product)
    )
      .then((res) => {
        setResponse("success");
        setLoading(false);
        res?.id && setProduct(ProductModel());
        toastify("success", "Operation successful");
        history.push("/admin/products");
      })
      .catch((err) => {
        toastify("error", "Operation failed");
        setResponse("failed");
        setLoading(false);
        console.log(err);
      });
  };

  return (
    <CCard className="my-2 mx-auto">
      <CCardHeader>
        {t("Product")} - {t(params.id ? "Edit" : "Create")}
      </CCardHeader>
      <CCardBody>
        {response && (
          <CAlert color={response === "success" ? "success" : "danger"}>
            {response}
          </CAlert>
        )}
        {loading ? (
          <div>Loading</div>
        ) : (
          <CForm>
            {error && <CAlert color="danger">Ошибка</CAlert>}
            <CNav variant="tabs" role="tablist">
              <CNavItem>
                <CNavLink
                  className="n-link"
                  active={activeKey === 1}
                  onClick={() => setActiveKey(1)}
                >
                  {t("Main")}
                </CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink
                  className="n-link"
                  active={activeKey === 2}
                  onClick={() => setActiveKey(2)}
                >
                  {t("Description")}
                </CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink
                  className="n-link"
                  active={activeKey === 3}
                  onClick={() => setActiveKey(3)}
                >
                  {t("Images")}
                </CNavLink>
              </CNavItem>
            </CNav>
            <CTabContent>
              <CTabPane
                className="pt-3 px-2"
                role="tabpanel"
                aria-labelledby="main-tab"
                visible={activeKey === 1}
              >
                <MainTab
                  product={product}
                  setProduct={setProduct}
                  colors={colors}
                  categories={categories}
                  marks={marks}
                />
              </CTabPane>
              <CTabPane
                className="pt-3"
                role="tabpanel"
                aria-labelledby="desc-tab"
                visible={activeKey === 2}
              >
                <DescriptionTab setProduct={setProduct} product={product} />
              </CTabPane>
              <CTabPane
                className="pt-3"
                role="tabpanel"
                aria-labelledby="images-tab"
                visible={activeKey === 3}
              >
                <ImagesTab
                  setImageUploading={setImageUploading}
                  setError={setError}
                  setProduct={setProduct}
                  product={product}
                />
              </CTabPane>
            </CTabContent>
          </CForm>
        )}
      </CCardBody>
      <CCardFooter className="d-flex flex-row-reverse">
        <CButton
          variant="outline"
          color="success"
          className="mx-1"
          disabled={loading || imageUploading}
          onClick={submitForm}
        >
          {t("Submit" + (loading ? "ing..." : ""))}
        </CButton>
        <CButton
          variant="outline"
          color="secondary"
          className="mx-1"
          disabled={loading}
          onClick={() => history.push("/admin/products")}
        >
          {t("Back")}
        </CButton>
      </CCardFooter>
    </CCard>
  );
}
