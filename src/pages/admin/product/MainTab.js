import {
  CCol,
  CFormCheck,
  CFormInput,
  CFormLabel,
  CFormSelect,
  CRow,
} from "@coreui/react";
import React from "react";
import { useTranslation } from "react-i18next";

import CreatableSelect from "react-select/creatable";
import Select from "react-select";
import { accessoriesSub, seasons, shoesSub } from "src/constants/menuList";

export default function MainTab({
  product,
  setProduct,
  colors,
  categories,
  marks,
}) {
  const { t } = useTranslation();

  const handleChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    setProduct({ ...product, [name]: value });
  };

  return (
    <div>
      <CRow>
        <CCol sm="4">
          <CFormLabel>{t("Name")}</CFormLabel>
          <CFormInput
            value={product.name}
            onChange={handleChange}
            type="text"
            placeholder="Name"
            name="name"
          />
          <CFormLabel>{t("Name")} (RU)</CFormLabel>
          <CFormInput
            value={product.name_ru}
            onChange={handleChange}
            type="text"
            placeholder="Name"
            name="name_ru"
          />

          <CFormLabel>{t("Link")}</CFormLabel>
          <CFormInput
            value={product.link}
            onChange={handleChange}
            type="text"
            placeholder="Link"
            name="link"
          />

          <CFormLabel>{t("Original Price")}</CFormLabel>
          <CFormInput
            value={product.priceOriginal}
            onChange={(e) => {
              console.log(product.price);
              if (product.price === 0) {
                setProduct({
                  ...product,
                  priceOriginal: e.target.value
                    .replace(/[^\d.,-]/g, "")
                    .replace(/[,]/g, "."),
                  price: e.target.value
                    .replace(/[^\d.,-]/g, "")
                    .replace(/[,]/g, "."),
                });
              } else {
                handleChange(e);
              }
            }}
            type="string"
            placeholder="Original price"
            name="priceOriginal"
          />

          <CFormLabel>{t("Price")}</CFormLabel>
          <CFormInput
            value={product.price}
            onChange={handleChange}
            type="number"
            placeholder="Price"
            name="price"
          />
        </CCol>
        <CCol sm="4">
          <CFormLabel>{t("Category")}</CFormLabel>
          <CFormSelect
            value={product.category}
            onChange={(e) => handleChange(e)}
            name="category"
          >
            <option>None</option>
            {categories.map((c, i) => (
              <option key={i}>{c.name}</option>
            ))}
          </CFormSelect>

          <CFormLabel>{t("Mark")}</CFormLabel>
          <CreatableSelect
            isClearable
            onChange={(res) =>
              setProduct({
                ...product,
                mark: { name: res.label, icon: res.icon || "" },
              })
            }
            options={marks.map((m) => {
              return { label: m.name, value: m.id, ...m };
            })}
            value={
              product.mark && {
                label: product.mark.name,
                value: product.mark.id || product.mark.name,
              }
            }
          />

          <CFormLabel>{t("Сolors")}</CFormLabel>
          <CreatableSelect
            isMulti
            onChange={(res) =>
              setProduct({ ...product, colors: res.map((r) => r.label) })
            }
            options={colors?.map((c) => {
              return { label: c.name, value: c.id, ...c };
            })}
            value={product.colors?.map((c) => {
              return { label: c, value: c };
            })}
          />
          <CFormLabel>{t("Сolors") + "(ru)"}</CFormLabel>
          <CreatableSelect
            isMulti
            onChange={(res) =>
              setProduct({
                ...product,
                colors_ru: res.map((r) => r.label),
              })
            }
            options={colors?.map((c) => {
              return { label: c.name, value: c.id, ...c };
            })}
            value={product.colors_ru?.map((c) => {
              return { label: c, value: c };
            })}
          />
          <CFormLabel>{t("Sizes")}</CFormLabel>
          <CreatableSelect
            isMulti
            onChange={(res) =>
              setProduct({ ...product, sizes: res.map((r) => r.label) })
            }
            options={[]}
            value={product.sizes.map((c) => {
              return { label: c, value: c };
            })}
          />
        </CCol>
        <CCol sm="4">
          <div className="mt-2">
            {/* <CFormLabel>{t("New Season")}?</CFormLabel> */}
            <CFormCheck
              className="ml-2"
              name="isNewSeason"
              onChange={(e) =>
                setProduct({ ...product, isNewSeason: e.target.checked })
              }
              checked={product.isNewSeason}
              label={t("New Season") + "?"}
            />
          </div>
          <div className="mt-2">
            {/* <CFormLabel>{t("In Promotions")}?</CFormLabel> */}
            <CFormCheck
              className="ml-2"
              name="isPromoted"
              onChange={(e) =>
                setProduct({ ...product, isPromoted: e.target.checked })
              }
              checked={product.isPromoted}
              label={t("In Promotions") + "?"}
            />
          </div>
          {(product.category.toLowerCase() === "shoes" ||
            product.category.toLowerCase() === "accessories") && (
            <>
              <CFormLabel>{t("Түрү")}</CFormLabel>
              <CFormSelect
                value={product.viewType}
                onChange={(e) => handleChange(e)}
                name="viewType"
              >
                {product.category.toLowerCase() === "shoes" ? (
                  <>
                    <option value={""}>{t("None")}</option>
                    {shoesSub.map((sub) => (
                      <option value={sub} key={sub}>
                        {t(sub)}
                      </option>
                    ))}
                  </>
                ) : (
                  <>
                    <option value={""}>{t("None")}</option>
                    {accessoriesSub.map((sub) => (
                      <option value={sub} key={sub}>
                        {t(sub)}
                      </option>
                    ))}
                  </>
                )}
              </CFormSelect>
            </>
          )}
          <CFormLabel>{t("Сезону")}</CFormLabel>
          <Select
            isMulti
            onChange={(res) =>
              setProduct({ ...product, seasons: res.map((r) => r.value) })
            }
            options={seasons.map((s) => {
              return { label: t(s), value: s };
            })}
            value={product.seasons?.map((c) => {
              return { label: t(c), value: c };
            })}
          />
        </CCol>
      </CRow>
    </div>
  );
}
