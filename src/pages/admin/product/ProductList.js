import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCardTitle,
  CCol,
  CFormInput,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CRow,
  CTable,
  CTableBody,
  CTableHead,
} from "@coreui/react";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { useTranslation } from "react-i18next";
import { toastify } from "src/helpers/toast";
import ProductService from "src/services/ProductService";

export default function ProductList() {
  const { t } = useTranslation();
  const [products, setProducts] = useState([]);
  const [confirmModal, setConfirmModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [searchKey, setSearchKey] = useState("");

  const history = useHistory();

  useEffect(() => {
    ProductService.getAll().then((d) => {
      setProducts(d);
    });
  }, []);

  console.log("pro", products);

  const deleteModal = (product) => {
    setConfirmModal(true);
    setSelectedProduct(product);
  };

  const confirmDelete = () => {
    ProductService.delete(selectedProduct.id)
      .then((res) => {
        console.log("deleted", res);
        toastify("success", t("Operation succeeded"));
        setConfirmModal(false);
      })
      .catch((err) => console.log(err));
  };

  const seasonConverter = (season) => {
    switch (season) {
      case "Кыш":
        return "Winter";
      case "Жаз":
        return "Spring";
      case "Жай":
        return "Summer";
      case "Күз":
        return "Fall";
      default:
        return "";
    }
  };

  const viewTypeConverter = (viewType) => {
    switch (viewType) {
      case "Спорттук":
        return "Sport";
      case "Күнүмдүк":
        return "Daily";
      default:
        return "";
    }
  };

  const updateProducts = () => {
    products.forEach((pro) => {
      let newSeasonValue = null;
      if (pro.season) {
        newSeasonValue = seasonConverter(pro.season);
        ProductService.update(pro.id, { ...pro, seasons: [newSeasonValue] })
          .then((res) => {
            toastify("success", t("Operation succeeded"));
          })
          .catch((err) => console.log("upd err"));
      }
      let newViewType = null;
      if (pro.viewType) {
        newViewType = viewTypeConverter(pro.viewType);
        ProductService.update(pro.id, { ...pro, viewType: newViewType })
          .then((res) => {
            toastify("success", t("Operation succeeded"));
          })
          .catch((err) => console.log("upd err"));
      }
    });
  };
  return (
    <CCard className="my-2 mx-auto">
      <CCardHeader>
        <CRow>
          <CCol>
            <CCardTitle>{t("Products")}</CCardTitle>
          </CCol>
          <CCol className="d-flex flex-row-reverse">
            <CButton
              className="float-right"
              color="success"
              onClick={() => history.push("/admin/product/create")}
            >
              {t("Create New")}
            </CButton>
            <CButton
              className="float-right mx-1 d-none"
              color="info"
              onClick={() => updateProducts()}
            >
              {t("Total Update")}
            </CButton>
          </CCol>
        </CRow>
      </CCardHeader>
      <CCardBody>
        <CFormInput
          className="my-2"
          placeholder="search..."
          onChange={(e) => setSearchKey(e.target.value)}
        />
        <div className="table-responsive">
          <CTable className="table">
            <CTableHead>
              <tr>
                <th></th>
                <th>{t("Name")}</th>
                <th>{t("Price")}</th>
                <th>{t("Mark")}</th>
                <th>{t("Category")}</th>
                <th>{t("Link")}</th>
                <th>{t("Actions")}</th>
              </tr>
            </CTableHead>
            <CTableBody>
              {products
                ?.filter((p) =>
                  searchKey
                    ? p.name?.toLowerCase().includes(searchKey.toLowerCase()) ||
                      p.name_ru?.toLowerCase().includes(searchKey.toLowerCase())
                    : true
                )
                .map((p, i) => (
                  <tr key={i}>
                    <td>{i + 1}</td>
                    <td>{p.name}</td>
                    <td>{p.price}</td>
                    <td>{p.mark?.name}</td>
                    <td>{p.category}</td>
                    <td>
                      <a href={p.link} target="_blank" rel="noreferrer">
                        {p.link && p.link.length > 10
                          ? p.link.substring(0, 10) + "..."
                          : p.link}
                      </a>
                    </td>
                    <td>
                      <CButton
                        onClick={() =>
                          history.push("/admin/product/edit/" + p.id)
                        }
                        className="mx-1 btn-sm"
                        variant="outline"
                        color="warning"
                      >
                        {t("Edit")}
                      </CButton>
                      <CButton
                        onClick={() => deleteModal(p)}
                        className="mx-1 btn-sm"
                        color="danger"
                        variant="outline"
                      >
                        {t("Delete")}
                      </CButton>
                      <CButton
                        onClick={() => history.push("/products/detail/" + p.id)}
                        className="mx-1 btn-sm"
                        color="info"
                        variant="outline"
                      >
                        {t("Details")}
                      </CButton>
                    </td>
                  </tr>
                ))}
            </CTableBody>
          </CTable>
          <CModal visible={confirmModal} onClose={() => setConfirmModal(false)}>
            <CModalHeader>{t("Confirm delete")}</CModalHeader>
            <CModalBody>
              {t("Are you sure to delete")} {selectedProduct?.name} ?
            </CModalBody>
            <CModalFooter>
              <CButton onClick={confirmDelete}>{t("Confirm")}</CButton>
            </CModalFooter>
          </CModal>
        </div>
      </CCardBody>
      <CCardFooter></CCardFooter>
    </CCard>
  );
}
