import { CBadge, CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCardTitle, CCol, CModal, CModalBody, CModalFooter, CModalHeader, CRow, CTable, CTableBody, CTableHead } from '@coreui/react'
import React, { useEffect, useState } from "react"
import { fire } from "src/configs/fire";
import { doc, onSnapshot, collection, query, deleteDoc, setDoc } from "firebase/firestore";
import { useHistory } from 'react-router';
import { useTranslation } from 'react-i18next';
import { toastify } from 'src/helpers/toast';
import { dateFormatter } from 'src/helpers/dateFormatter';

export default function Problems() {

    const {t} = useTranslation()
    const [problems, setProblems] = useState([])

    const [confirmModal, setConfirmModal] = useState(false)
    const [selectedItem, setSelectedItem] = useState(null)

    const [status, setStatus] = useState(null)

    const history = useHistory()

    useEffect(() => {
        const q = query(collection(fire, "problem"))
        onSnapshot(q, (querySnapshot) => {
            setProblems(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
          console.log("Data", querySnapshot.docs.map(d => d.data()));
        })
    }, [])

    const deleteModal = (problem) => {
        setConfirmModal(true)
        setSelectedItem(problem)
    }

    const confirmDelete = () => {
        deleteDoc(doc(fire, "problem", selectedItem.id)).then(res => {
            console.log("deleted", res)
            setConfirmModal(false)
        }).catch(err => console.log(err))
    }

    const setSolved = (problem) => {
        setDoc(doc(fire, "problem", problem.id), {status: "закрыто"}, {merge: true}).then(res => {
            toastify("success", "Operation successful")
        }).catch(err => {
            toastify("error", "Operation failed")
            console.log(err)
        })
    }


    return (
        <CCard className="my-2 mx-auto">
            <CCardHeader>
                <CRow>
                    <CCol>
                        <CCardTitle>
                            {t("Проблемы")}
                        </CCardTitle>
                    </CCol>
                    <CCol className="d-flex flex-row-reverse">
                        <CButton className="float-right" color="success" onClick={() => history.push("/admin/problem/create")}>
                            {t("Create New")}
                        </CButton>
                    </CCol>
                </CRow>
            </CCardHeader>
            <CCardBody>
                <CButton variant="outline" className='mx-1' onClick={() => setStatus(null)} color="primary">Все</CButton>
                <CButton variant="outline" className='mx-1' onClick={() => setStatus("создан")} color="success">Создан</CButton>
                <CButton variant="outline" className='mx-1' onClick={() => setStatus("закрыто")} color="info">Закрыто</CButton>
                <div className="table-responsive">
                <CTable className="table">
                    <CTableHead>
                        <tr>
                            <th>{t("Дата создания")}</th>
                            <th>{t("Статус")}</th>
                            <th>{t("Description")}</th>
                            <th>{t("Actions")}</th>
                        </tr>
                    </CTableHead>
                    <CTableBody>
                        {problems.filter(p => status ? p.status === status : true).map((c, i) => 
                            <tr key={i}>
                                <td>{dateFormatter(c.createdAt?.toDate())}</td>
                                <td>
                                    <CBadge color={c.status === "создан" ? "success" : "secondary"}>{c.status}</CBadge>
                                </td>
                                <td>{c.description}</td>
                                <td>
                                    <CButton onClick={() => history.push("/admin/problem/edit/" + c.id)} className="m-1 btn-sm"
                                        variant="outline" color="warning">{t("Edit")}</CButton>
                                    <CButton onClick={() => deleteModal(c)} className="m-1 btn-sm d-none"
                                        color="danger" variant="outline">{t("Delete")}</CButton>
                                    {c.status !== "закрыто" && <CButton onClick={() => setSolved(c)} className="m-1 btn-sm"
                                        color="secondary" variant="outline">{t("Close")}</CButton>}
                                </td>
                            </tr>
                            )}
                    </CTableBody>
                </CTable>
                <CModal visible={confirmModal} onClose={() => setConfirmModal(false)}>
                    <CModalHeader>{t("Confirm delete")}</CModalHeader>
                    <CModalBody>
                        {t("Are you sure to delete")} {selectedItem?.name} ?
                    </CModalBody>
                    <CModalFooter>
                        <CButton onClick={confirmDelete}>{t("Confirm")}</CButton>
                    </CModalFooter>
                </CModal>
                </div>
            </CCardBody>
            <CCardFooter>

            </CCardFooter>
        </CCard>
    )
}