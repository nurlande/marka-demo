import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCardTitle, CCol, CModal, CModalBody, CModalFooter, CModalHeader, CRow, CTable, CTableBody, CTableHead } from '@coreui/react'
import React, { useEffect, useState } from "react"
import { fire } from "src/configs/fire";
import { onSnapshot, collection, query, setDoc, doc } from "firebase/firestore";
import { useTranslation } from 'react-i18next';
import { toastify } from 'src/helpers/toast';

export default function OrderList() {

    const {t} = useTranslation()
    
    const [orders, setOrders] = useState([])

    const [selectedItem, setSelectedItem] = useState(null)

    useEffect(() => {
        const q = query(collection(fire, "orders"))
        onSnapshot(q, (querySnapshot) => {
            setOrders(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
          console.log("Data", querySnapshot.docs.map(d => d.data()));
        })
    }, [])

    const [show, setShow] = useState(false)

    const showDetails = order => {
        setSelectedItem(order)
        setShow(true)
    }

    const approveOrder = () => {
        setDoc(doc(fire, "orders", selectedItem.id), {status: "approved"}, {merge: true}).then(res => {
            toastify("success", "Operation successful")
        }).catch(err => {
            toastify("error", "Operation failed")
            console.log(err)
        })
    }

    return (
        <CCard className="my-2 mx-auto">
            <CCardHeader>
                <CRow>
                    <CCol>
                        <CCardTitle>
                            {t("Orders")}
                        </CCardTitle>
                    </CCol>
                    {/* <CCol className="d-flex flex-row-reverse">
                        <CButton className="float-right" color="success" onClick={() => history.push("/admin/category/create")}>
                            Create New
                        </CButton>
                    </CCol> */}
                </CRow>
            </CCardHeader>
            <CCardBody>
                <div className="table-responsive">
                <CTable className="table">
                    <CTableHead>
                        <tr>
                            <th>ID</th>
                            <th>{t("Receiver Name")}</th>
                            <th>{t("Receiver Phone")}</th>
                            <th>{t("Description")}</th>
                            <th>{t("Total price")}</th>
                            <th>{t("Status")}</th>
                        </tr>
                    </CTableHead>
                    <CTableBody>
                        {orders.map((o, i) => 
                            <tr key={i}>
                                <td>{o.id}</td>
                                <td>{o.receiverName}</td>
                                <td>{o.receiverPhone}</td>
                                <td>{o.description ? o.description.substring(0, 10) + "..." : o.description}</td>
                                <td>
                                    <CButton onClick={() => showDetails(o)} className="mx-1 btn-sm"
                                        color="info" variant="outline">{t("Details")}</CButton>
                                </td>
                                <td>{o.status}</td>
                            </tr>
                            )}
                    </CTableBody>
                </CTable>
                <CModal visible={show} onClose={() => setShow(false)} size="xl">
                    <CModalHeader>{t("Details")}</CModalHeader>
                    <CModalBody>
                        <div className="table-responsive">
                            <CTable className="table">
                                <CTableHead>
                                    <tr>
                                        <th>{t("Name")}</th>
                                        <th>{t("Color")}</th>
                                        <th>{t("Size")}</th>
                                        <th>{t("Amount")}</th>
                                        <th>{t("Link")}</th>
                                    </tr>
                                </CTableHead>
                                <CTableBody>
                                {selectedItem?.products?.map((p, i) => 
                                    <tr key={i}>
                                        <td>{p.name}</td>
                                        <td>{p.selectedColor}</td>
                                        <td>{p.selectedSize}</td>
                                        <td>{p.amount}</td>
                                        <td><a href={p.link} target="_blank" rel="noreferrer">{p.link}</a></td>
                                    </tr>
                                    )}
                                </CTableBody>
                            </CTable>
                        </div>
                    </CModalBody>
                    <CModalFooter>
                        <CButton onClick={() => approveOrder()} color="success">Кабыл алуу</CButton>
                        <CButton onClick={() => setShow(false)}>{t("Close")}</CButton>
                    </CModalFooter>
                </CModal>
                </div>
            </CCardBody>
            <CCardFooter>

            </CCardFooter>
        </CCard>
    )
}