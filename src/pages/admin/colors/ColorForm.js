import { CAlert, CButton, CCard, CCardBody, 
    CCardFooter, CCardHeader, CForm, 
    CFormInput, CFormLabel, 
     } from '@coreui/react'
import React, { useEffect, useState } from 'react'
import { collection, addDoc, doc, setDoc, getDoc } from "firebase/firestore";
import { fire } from "src/configs/fire";
import { useHistory, useParams } from 'react-router';
import { toastify } from 'src/helpers/toast';
import { useTranslation } from 'react-i18next';


export default function ColorForm() {

    const {t} = useTranslation()

    const params = useParams()
    const history = useHistory()

    const [color, setColor] = useState({
        name: "",
        code: "",
    })

    const [loading, setLoading] = useState(false)
    const [response, setResponse] = useState(null)
    
    const [docRef, setDocRef] = useState(null)

    useEffect(() => {
        if(params.id) {
            const dRef = doc(fire, "colors", params.id)
            setDocRef(dRef)
            getDoc(dRef).then(res => setColor(res.data()))
        }
    }, [params])

    const handleChange = (e) => {
        let name = e.target.name
        let value = e.target.value
        setColor({...color, [name]: value})
    }

    const submitForm = (e) => {
        e.preventDefault()
        setLoading(true)
        let payload = color
        console.log(payload)
        const collectionRef = collection(fire, "colors");
        (params.id ? setDoc(docRef, payload) : addDoc(collectionRef, payload)).then(res => {
            setResponse("success")
            setLoading(false)
            res?.id && setColor({})
            history.push("/admin/colors")
            toastify("success", "Operation successful")
            // add redirect
        }).catch(err => {
            toastify("error", "Operation failed")
            setResponse("failed")
            setLoading(false)
            console.log(err)
        })
    }

    return (
        <CCard className="my-2 w-75 mx-auto">
            <CCardHeader>
                {t("Colors")} - {t(params.id ? "Edit" : "Create")}
            </CCardHeader>
            <CCardBody>
                
                {response && <CAlert color={response === "success" ? "success" : "danger"}>{response}</CAlert>}
                {loading ? 
                    <div>Loading</div> : 
                    <CForm>
                        <CFormLabel>{t("Name")}</CFormLabel>
                        <CFormInput value={color.name} onChange={handleChange} type="text" placeholder="Name" name="name"/>
                        
                        <CFormLabel>{t("Code")}</CFormLabel>
                        <CFormInput value={color.code} onChange={handleChange} type="text" placeholder="Code" name="code"/>

                    </CForm>}
            </CCardBody>
            <CCardFooter className="d-flex flex-row-reverse">
            <CButton variant="outline" color="success" className="mx-1" 
                        disabled={loading} onClick={submitForm}>{t("Submit" + (loading ? "ing..." : ""))}</CButton>
                    <CButton variant="outline" color="secondary" className="mx-1"
                        disabled={loading} onClick={() => history.push("/admin/marks")}>{t("Back")}</CButton>
            </CCardFooter>
        </CCard>
    )
}