import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCardTitle, CCol, CModal, CModalBody, CModalFooter, CModalHeader, CRow, CTable, CTableBody, CTableHead } from '@coreui/react'
import React, { useEffect, useState } from "react"
import { fire } from "src/configs/fire";
import { doc, onSnapshot, collection, query, deleteDoc } from "firebase/firestore";
import { useHistory } from 'react-router';
import { useTranslation } from 'react-i18next';

export default function Colors() {

    const {t} = useTranslation()
    const [colors, setColors] = useState([])

    const [confirmModal, setConfirmModal] = useState(false)
    const [selectedItem, setSelectedItem] = useState(null)

    const history = useHistory()

    useEffect(() => {
        const q = query(collection(fire, "colors"))
        onSnapshot(q, (querySnapshot) => {
            setColors(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
          console.log("Data", querySnapshot.docs.map(d => d.data()));
        })
    }, [])

    const deleteModal = (color) => {
        setConfirmModal(true)
        setSelectedItem(color)
    }

    const confirmDelete = () => {
        deleteDoc(doc(fire, "colors", selectedItem.id)).then(res => {
            console.log("deleted", res)
            setConfirmModal(false)
        }).catch(err => console.log(err))
    }

    return (
        <CCard className="my-2 mx-auto">
            <CCardHeader>
                <CRow>
                    <CCol>
                        <CCardTitle>
                            {t("Colors")}
                        </CCardTitle>
                    </CCol>
                    <CCol className="d-flex flex-row-reverse">
                        <CButton className="float-right" color="success" onClick={() => history.push("/admin/color/create")}>
                            {t("Create New")}
                        </CButton>
                    </CCol>
                </CRow>
            </CCardHeader>
            <CCardBody>
                <div className="table-responsive">
                <CTable className="table">
                    <CTableHead>
                        <tr>
                            <th>{t("Name")}</th>
                            <th>{t("Code")}</th>
                            <th>{t("Actions")}</th>
                        </tr>
                    </CTableHead>
                    <CTableBody>
                        {colors.map((c, i) => 
                            <tr key={i}>
                                <td>{c.name}</td>
                                <td>
                                    {c.code}
                                </td>
                                <td>
                                    <CButton onClick={() => history.push("/admin/color/edit/" + c.id)} className="mx-1 btn-sm"
                                        variant="outline" color="warning">{t("Edit")}</CButton>
                                    <CButton onClick={() => deleteModal(c)} className="mx-1 btn-sm"
                                        color="danger" variant="outline">{t("Delete")}</CButton>
                                </td>
                            </tr>
                            )}
                    </CTableBody>
                </CTable>
                <CModal visible={confirmModal} onClose={() => setConfirmModal(false)}>
                    <CModalHeader>{t("Confirm delete")}</CModalHeader>
                    <CModalBody>
                        {t("Are you sure to delete")} {selectedItem?.name} ?
                    </CModalBody>
                    <CModalFooter>
                        <CButton onClick={confirmDelete}>{t("Confirm")}</CButton>
                    </CModalFooter>
                </CModal>
                </div>
            </CCardBody>
            <CCardFooter>

            </CCardFooter>
        </CCard>
    )
}