import { CButton, CCard, CCardBody, CCardFooter, CCardHeader, CCardTitle, CCol, CModal, CModalBody, CModalFooter, CModalHeader, CRow, CTable, CTableBody, CTableHead } from '@coreui/react'
import React, { useEffect, useState } from "react"
import { fire } from "src/configs/fire";
import { doc, onSnapshot, collection, query, deleteDoc } from "firebase/firestore";
import { useHistory } from 'react-router';
import { useTranslation } from 'react-i18next';

export default function MarkList() {

    const {t} = useTranslation()
    const [marks, setMarks] = useState([])

    const [confirmModal, setConfirmModal] = useState(false)
    const [selectedItem, setSelectedItem] = useState(null)

    const history = useHistory()

    useEffect(() => {
        const q = query(collection(fire, "mark"))
        onSnapshot(q, (querySnapshot) => {
            setMarks(querySnapshot.docs.map(d => {
                return {id: d.id, ...d.data()}
            }))
          console.log("Data", querySnapshot.docs.map(d => d.data()));
        })
    }, [])

    const deleteModal = (mark) => {
        setConfirmModal(true)
        setSelectedItem(mark)
    }

    const confirmDelete = () => {
        deleteDoc(doc(fire, "mark", selectedItem.id)).then(res => {
            console.log("deleted", res)
            setConfirmModal(false)
        }).catch(err => console.log(err))
    }

    return (
        <CCard className="my-2 mx-auto">
            <CCardHeader>
                <CRow>
                    <CCol>
                        <CCardTitle>
                            {t("Marks")}
                        </CCardTitle>
                    </CCol>
                    <CCol className="d-flex flex-row-reverse">
                        <CButton className="float-right" color="success" onClick={() => history.push("/admin/mark/create")}>
                            {t("Create New")}
                        </CButton>
                    </CCol>
                </CRow>
            </CCardHeader>
            <CCardBody>
                <div className="table-responsive">
                <CTable className="table">
                    <CTableHead>
                        <tr>
                            <th>{t("Name")}</th>
                            <th>{t("Icon")}</th>
                            <th>{t("Actions")}</th>
                        </tr>
                    </CTableHead>
                    <CTableBody>
                        {marks.map((c, i) => 
                            <tr key={i}>
                                <td>{c.name}</td>
                                <td>
                                    <img alt="" src={c.icon} width="60"/>
                                </td>
                                <td>
                                    <CButton onClick={() => history.push("/admin/mark/edit/" + c.id)} className="mx-1 btn-sm"
                                        variant="outline" color="warning">{t("Edit")}</CButton>
                                    <CButton onClick={() => deleteModal(c)} className="mx-1 btn-sm"
                                        color="danger" variant="outline">{t("Delete")}</CButton>
                                    {/* <CButton onClick={() => history.push("/admin/category/details/" + c.id)} className="mx-1 btn-sm"
                                        color="info" variant="outline">Details</CButton> */}
                                </td>
                            </tr>
                            )}
                    </CTableBody>
                </CTable>
                <CModal visible={confirmModal} onClose={() => setConfirmModal(false)}>
                    <CModalHeader>{t("Confirm delete")}</CModalHeader>
                    <CModalBody>
                        {t("Are you sure to delete")} {selectedItem?.name} ?
                    </CModalBody>
                    <CModalFooter>
                        <CButton onClick={confirmDelete}>{t("Confirm")}</CButton>
                    </CModalFooter>
                </CModal>
                </div>
            </CCardBody>
            <CCardFooter>

            </CCardFooter>
        </CCard>
    )
}