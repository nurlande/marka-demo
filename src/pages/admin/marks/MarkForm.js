import { CAlert, CButton, CCard, CCardBody, 
    CCardFooter, CCardHeader, CForm, 
    CFormInput, CFormLabel, 
     } from '@coreui/react'
import React, { useEffect, useState } from 'react'
import { collection, addDoc, doc, setDoc, getDoc } from "firebase/firestore";
import { fire, fireStorage} from "src/configs/fire";
import { useHistory, useParams } from 'react-router';
import { toastify } from 'src/helpers/toast';
import { useTranslation } from 'react-i18next';
import { getDownloadURL, ref, uploadBytesResumable } from '@firebase/storage';


export default function MarkForm() {

    const {t} = useTranslation()

    const params = useParams()
    const history = useHistory()

    const [mark, setMark] = useState({
        name: "",
        icon: "",
    })

    const [loading, setLoading] = useState(false)
    const [response, setResponse] = useState(null)
    
    const [docRef, setDocRef] = useState(null)

    useEffect(() => {
        if(params.id) {
            const dRef = doc(fire, "mark", params.id)
            setDocRef(dRef)
            getDoc(dRef).then(res => setMark(res.data()))
        }
    }, [params])

    const handleChange = (e) => {
        let name = e.target.name
        let value = e.target.value
        setMark({...mark, [name]: value})
    }

    const submitForm = (e) => {
        e.preventDefault()
        setLoading(true)
        let payload = mark
        console.log(payload)
        const collectionRef = collection(fire, "mark");
        (params.id ? setDoc(docRef, payload) : addDoc(collectionRef, payload)).then(res => {
            setResponse("success")
            setLoading(false)
            res?.id && setMark({})
            history.push("/admin/marks")
            toastify("success", "Operation successful")
            // add redirect
        }).catch(err => {
            toastify("error", "Operation failed")
            setResponse("failed")
            setLoading(false)
            console.log(err)
        })
    }

    const [progress, setProgress] = useState(null)

    const uploadImage = (e) => {
        const image = e.target.files[0]
        console.log(image)

        const storageRef = ref(fireStorage, `images/${image.name}`)
        const uploadTask = uploadBytesResumable(storageRef, image)
        uploadTask.on(
        "state_changed",
        snapshot => {
            const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            );
            setProgress(progress);
        },
        error => {
            console.log(error);
        },
        () => {
            getDownloadURL(uploadTask.snapshot.ref).then(url => {
                setMark({...mark, "icon": url})
            })
        })
    }

    console.log(mark)
    return (
        <CCard className="my-2 w-75 mx-auto">
            <CCardHeader>
                {t("Marks")} - {t(params.id ? "Edit" : "Create")}
            </CCardHeader>
            <CCardBody>
                
                {response && <CAlert color={response === "success" ? "success" : "danger"}>{response}</CAlert>}
                {loading ? 
                    <div>Loading</div> : 
                    <CForm>
                        <CFormLabel>{t("Name")}</CFormLabel>
                        <CFormInput value={mark.name} onChange={handleChange} type="text" placeholder="Name" name="name"/>
                        
                        <CFormLabel>{t("Image")} URL</CFormLabel>
                        <CFormInput value={mark.icon} onChange={handleChange} type="text" placeholder="Image url" name="icon"/>


                        <CFormLabel>{t("Image upload")}</CFormLabel>
                        <CFormInput onChange={e => uploadImage(e)} type="file" name="file"/>
                        {progress ? progress + " %" : ""}

                    </CForm>}
            </CCardBody>
            <CCardFooter className="d-flex flex-row-reverse">
            <CButton variant="outline" color="success" className="mx-1" 
                        disabled={loading} onClick={submitForm}>{t("Submit" + (loading ? "ing..." : ""))}</CButton>
                    <CButton variant="outline" color="secondary" className="mx-1"
                        disabled={loading} onClick={() => history.push("/admin/marks")}>{t("Back")}</CButton>
            </CCardFooter>
        </CCard>
    )
}