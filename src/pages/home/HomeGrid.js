import { CCol, CRow } from "@coreui/react";
import React from "react";
import { useHistory } from "react-router-dom";
import banner_bag from "src/assets/banner_bag.jpg";
import banner_shoes1 from "src/assets/banner_shoes1.jpg";
import banner_shoes2 from "src/assets/banner_shoes2.jpg";
import banner_shoes3 from "src/assets/banner_shoes3.jpg";
// import ab1 from "src/assets/ab1.JPG";
import { useTranslation } from "react-i18next";

const gridMenu = [
  {
    link: "/shoes/HighHeel",
    image: banner_shoes1,
    label: "High Heels shoes",
  },
  {
    link: "/shoes/Sport",
    image: banner_shoes2,
    label: "Sport shoes",
  },
  {
    link: "/shoes/Daily",
    image: banner_shoes3,
    label: "Daily shoes",
  },
  {
    link: "/products/bag",
    image: banner_bag,
    label: "Bag",
  },
  // {
  //   link: "/products/accessories",
  //   image: ab1,
  //   label: "Accessories",
  // },
];
export default function HomeGrid() {
  const history = useHistory();
  const { t } = useTranslation();

  return (
    <CRow>
      {gridMenu.map((menu, i) => (
        <CCol xs="6" sm="4" md="3" key={i}>
          <div
            className="my-2"
            style={{
              position: "relative",
              cursor: "pointer",
            }}
            onClick={() => history.push(menu.link)}
          >
            <img
              alt="grid"
              src={menu.image}
              style={{
                objectFit: "cover",
                width: "100%",
                height: "100%",
                borderRadius: "10px",
              }}
            />
            <div
              style={{
                position: "absolute",
                bottom: 5,
                left: 10,
                color: "Black",
              }}
              className="banner-text"
            >
              {t(menu.label)}
            </div>
          </div>
        </CCol>
      ))}
    </CRow>
  );
}
