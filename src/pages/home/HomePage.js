import { CButton, CCol, CRow } from "@coreui/react";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import ProductGrid from "../products/ProductGrid";
import Marquee from "react-fast-marquee";
import HomeGrid from "./HomeGrid";
import banny1 from "src/assets/banny1.PNG";
import banny2 from "src/assets/banny2.jpg";
// import banny3 from "src/assets/banny3.PNG";
import banny3min from "src/assets/banny3-compressed.jpg";
import "./Home.scss";
import { useTranslation } from "react-i18next";

export default function HomePage() {
  const history = useHistory();
  const params = useParams();
  const { t } = useTranslation();
  const products = useSelector((state) => state.product.products);

  const [randomPro, setRandomPro] = useState(null);
  const [maxSize, setMaxSize] = useState(15);

  useEffect(() => {
    setRandomPro(products[Math.floor(Math.random() * products.length)]);
  }, [products]);

  return (
    <>
      {params.banner ? (
        <img
          style={{ width: "100%" }}
          alt="main banner"
          src={params.banner === "1" ? banny1 : banny2}
        />
      ) : (
        <div
          className="w-100 mx-auto home-banner"
          style={{
            backgroundImage: `url(${banny3min})`,
          }}
        >
          <Marquee gradient={false} className="marquee-div">
            <div className="marquee-text" style={{ marginRight: "8px" }}>
              {t("Сапаттуу бут кийимдер дүйнөсү")}
            </div>
          </Marquee>
        </div>
      )}
      <div className="mx-1">
        <HomeGrid />
      </div>

      <CRow className="d-none">
        <CCol sm="8">
          <ProductGrid
            categoryFilter={"all"}
            products={products}
            grid={4}
            maxSize={maxSize}
          />
          <div className="text-center">
            {products.length >= maxSize && (
              <CButton
                className="my-3 text-primary"
                variant="ghost"
                color="white"
                size="sm"
                onClick={() => setMaxSize((size) => size + 15)}
              >
                {t("Show More")}
              </CButton>
            )}
          </div>
        </CCol>
        <CCol sm="4" className="text-center">
          <h5>{t("Most Sold")}</h5>
          {randomPro && (
            <div
              className="mx-5"
              style={{ cursor: "pointer" }}
              onClick={() => history.push("/products/detail/" + randomPro?.id)}
            >
              <img
                alt=""
                src={
                  randomPro.image ||
                  (randomPro.images ? randomPro.images[0] : null)
                }
                style={{ maxWidth: "100%" }}
              />
              <h6>{randomPro.name}</h6>
            </div>
          )}
        </CCol>
      </CRow>
    </>
  );
}
