import React, {useState} from 'react'
import {
  CAlert,
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import {signInWithEmailAndPassword, onAuthStateChanged} from "@firebase/auth"
import { fireAuth } from 'src/configs/fire'
import {useHistory} from 'react-router-dom'

const Login = () => {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("")

  const [response, setResponse] = useState(null)

  const history = useHistory()

  onAuthStateChanged(fireAuth, currentUser => {
    localStorage.setItem("markaUser", JSON.stringify(currentUser))
})

const submitLogin = (e) => {
    e.preventDefault()
    const user = signInWithEmailAndPassword(fireAuth, email, password)
    user.then(u => {
        setResponse("success")
        console.log("success", u)
        history.push("/home")
    }).catch(err => {
      console.log("error", err)
      setResponse("error")
    })
}

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={8}>
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                {response && <CAlert className="text-capitalize" color={response === "success" ? "success" : "danger"}>
                  {response}
                </CAlert>}
                  <CForm onSubmit={submitLogin}>
                    <h1>Логин</h1>
                    <p className="text-medium-emphasis">Кирүү</p>
                    <CInputGroup className="mb-3">
                      <CInputGroupText>
                        <CIcon icon={cilUser} />
                      </CInputGroupText>
                      <CFormInput placeholder="email" autoComplete="email" onChange={e => setEmail(e.target.value)}/>
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupText>
                        <CIcon icon={cilLockLocked} />
                      </CInputGroupText>
                      <CFormInput
                        type="password"
                        placeholder="Password"
                        autoComplete="current-password"
                        onChange={e => setPassword(e.target.value)}
                      />
                    </CInputGroup>
                    <CRow >
                      <CCol xs={6}>
                        <CButton color="link" className="px-0" onClick={() => history.push("/register")}>
                          Аккаунт түзүү
                        </CButton>
                      </CCol>
                      <CCol xs={6} className="d-flex flex-row-reverse">
                        <CButton color="primary" className="px-4" type="submit">
                          Кирүү
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
