import React, {useEffect, useState} from 'react'
import {
  CAlert,
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked } from '@coreui/icons'
import {createUserWithEmailAndPassword, onAuthStateChanged} from "@firebase/auth"
import { fire, fireAuth } from 'src/configs/fire'
import { useHistory } from 'react-router'
import { collection, doc, setDoc } from 'firebase/firestore'

const Register = () => {

    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("")
    const [password, setPassword] = useState("")
    const [passwordRepeat, setPasswordRepeat] = useState("")

    const [inValid, setInValid] = useState(null)
    const [response, setResponse] = useState(null)

    onAuthStateChanged(fireAuth, currentUser => {
        localStorage.setItem("markaUser", JSON.stringify(currentUser))
    })

    const history = useHistory()


    useEffect(() => {
      if(password !== passwordRepeat) {
        setInValid(true)
      } else {
        setInValid(false)
      }
    }, [password, passwordRepeat])

    const submitRegister = (e) => {
        e.preventDefault()
        const user = createUserWithEmailAndPassword(fireAuth, email, password)
        user.then(u => {
          setDoc(doc(collection(fire, "users"), u.user?.uid), {uid: u.user?.uid, email, password, role: "client", phone}).then(res => {
            history.push("/home")
            setResponse("success")
            console.log(res)
          }).catch(err => console.log(err))
          console.log(u)
        }).catch(err => {
          setResponse("error")
          console.log(err)

        })
    }

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={9} lg={7} xl={6}>
            <CCard className="mx-4">
              <CCardBody className="p-4">

                {response && 
                  <CAlert color={response === "success" ? "success" : "danger"} className="text-capitalize">
                    {response}
                  </CAlert>}
                <CForm onSubmit={submitRegister}>
                  <h1>Регистрация</h1>
                  <p className="text-medium-emphasis">Жаңы аккаунт түзүү</p>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>Email</CInputGroupText>
                    <CFormInput placeholder="Email" autoComplete="email" onChange={e => setEmail(e.target.value)}/>
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>Телефон</CInputGroupText>
                    <CFormInput placeholder="телефон" autoComplete="tel" onChange={e => setPhone(e.target.value)}/>
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      placeholder="Password"
                      autoComplete="new-password"
                      onChange={e => setPassword(e.target.value)}
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-4">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      type="password"
                      placeholder="Repeat password"
                      autoComplete="new-password"
                      onChange={e => setPasswordRepeat(e.target.value)}
                    />
                  </CInputGroup>
                  {inValid && <CAlert color='danger'>Пароли не совпадают</CAlert>}
                  <div className="d-grid">
                    <CButton color="success" type="submit">Аккаунт түзүү</CButton>
                  </div>
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Register
