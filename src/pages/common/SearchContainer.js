import { CFormInput } from "@coreui/react";
import { useTranslation } from "react-i18next";

export default function SearchContainer({ onChange, value }) {
  const { t } = useTranslation();
  return (
    <div>
      <CFormInput
        placeholder={t("What are you searching?")}
        onChange={(e) => onChange(e.target.value)}
        value={value}
      />
    </div>
  );
}
