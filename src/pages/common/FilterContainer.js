import {
  CButton,
  CFormInput,
  CFormLabel,
  CModal,
  CModalBody,
} from "@coreui/react";
import { useTranslation } from "react-i18next";

export default function FilterContainer({ onClose, show, range, setRange }) {
  const { t } = useTranslation();
  return (
    <CModal backdrop="static" visible={show} onClose={onClose}>
      <CModalBody>
        <CFormLabel>{t("Min price")}</CFormLabel>
        <CFormInput
          type="number"
          value={range.min}
          onChange={(e) =>
            setRange({ ...range, min: parseFloat(e.target.value) })
          }
        />
        <CFormLabel>{t("Max price")}</CFormLabel>
        <CFormInput
          type="number"
          value={range.max}
          onChange={(e) =>
            setRange({ ...range, max: parseFloat(e.target.value) })
          }
        />
        <div className="d-flex justify-content-end">
          <CButton className="mt-2" onClick={onClose}>
            OK
          </CButton>
        </div>
      </CModalBody>
    </CModal>
  );
}
