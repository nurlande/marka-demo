import { CCard, CCardBody, CCardHeader } from '@coreui/react'
import React from "react"
import { useTranslation } from 'react-i18next'

export default function PersonalRoom() {

    const {t} = useTranslation()

    return (
        <CCard className="my-2 mx-auto">
            <CCardHeader>
                {t("Личный кабинет")}
            </CCardHeader>
            <CCardBody>
                Personal Room
            </CCardBody>
        </CCard>
    )
}