import { ADDTOCART, REMOVEFROMCART, EMPTYCART, UPDATECART, GET_CATEGORIES, GET_PRODUCTS } from '../types/productTypes'

const initialState = {
  cart: [],
  categories: [],
  products: []
}

export default function productReducer(state = initialState, action) {
  switch (action.type) {
    case ADDTOCART:
      return { ...state, cart: [...state.cart, {amount: 1, ...action.payload}]}
    
    case REMOVEFROMCART:
      return { ...state, cart: [...state.cart].filter(c => c.id !== action.payload.id)}
    
    case EMPTYCART:
      return { ...state, cart: []}

    case UPDATECART:
      let allCart = state.cart
      allCart[allCart.findIndex(c => c.id === action.id)].amount = action.amount
      return { ...state, cart: [...allCart]}

    case GET_CATEGORIES:
      return { ...state, categories: action.payload}

    case GET_PRODUCTS:
      return { ...state, products: action.payload}
    
    default:
      return state
  }
}
