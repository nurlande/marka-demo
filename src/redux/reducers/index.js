import { combineReducers } from 'redux'
import authReducer from './authReducer'
import productReducer from './productReducer'
import settingsReducer from './settingsReducer'

export default combineReducers({
  settings: settingsReducer,
  auth: authReducer,
  product: productReducer
})
