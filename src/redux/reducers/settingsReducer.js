import { SET, UNFOLD, MODE } from '../types/settingsTypes'

const initialState = {
  sidebarShow: false,
  sidebarUnfoldable : false,
  mode: "client"
}

export default function settingsReducer(state = initialState, action) {
  switch (action.type) {
    case SET:
      return { ...state, sidebarShow: action.payload }
    case UNFOLD:
      return { ...state, sidebarUnfoldable: action.payload }
    case MODE:
      return { ...state, mode: action.payload }
    default:
      return state
  }
}
