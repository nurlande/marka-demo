import { SET, MODE, UNFOLD} from '../types/settingsTypes'

export const setSidebarShow = sidebarShow => ({ type: SET, payload: sidebarShow })

export const setSidebarUnfold = sidebarUnfoldable => ({ type: UNFOLD, payload: sidebarUnfoldable })

export const setMode = mode => ({ type: MODE, payload: mode })
