import { ADDTOCART, EMPTYCART, REMOVEFROMCART, UPDATECART, GET_CATEGORIES, GET_PRODUCTS } from '../types/productTypes'

export const getCategories = categories => ({ type: GET_CATEGORIES, payload: categories })

export const getProducts = products => ({ type: GET_PRODUCTS, payload: products })

export const addToCart = product => ({ type: ADDTOCART, payload: product })

export const removeFromCart = product => ({ type: REMOVEFROMCART, payload: product })

export const emptyCart = () => ({ type: EMPTYCART })

export const updateAmountCart = (id, amount) => ({ type: UPDATECART, id: id, amount: amount })

