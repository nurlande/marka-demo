export const dateFormatter = (date) => {
    let d = new Date(date)
    let dateString = d.getDate() + "/" + (parseInt(d.getMonth()) + 1) + "/" + d.getFullYear() + "  " + (d.getHours() < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() < 10 ? "0" : "") + d.getMinutes() 
    return date ? dateString : ""
}