import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { setMode } from "src/redux/actions/settingsActions";
import {
  AppContent,
  AppSidebar,
  AppFooter,
  AppHeader,
} from "../components/index";

import { CSpinner } from "@coreui/react";

import { fire, fireAuth } from "src/configs/fire";
import { onSnapshot, collection, query } from "firebase/firestore";
import { getCategories, getProducts } from "src/redux/actions/productActions";
import { onAuthStateChanged } from "firebase/auth";

const DefaultLayout = () => {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState({ products: true, categories: true });

  useEffect(() => {
    const auth = fireAuth;
    onAuthStateChanged(auth, (user) => {
      dispatch(
        user?.uid === "qLbN4mhgUzewWdNoRweHW9ep0cq1"
          ? setMode("admin")
          : setMode("client")
      );
    });
  }, [dispatch]);

  useEffect(() => {
    const qc = query(collection(fire, "category"));
    onSnapshot(qc, (querySnapshot) => {
      setLoading((loads) => {
        return { ...loads, categories: false };
      });
      dispatch(
        getCategories(
          querySnapshot.docs.map((d) => {
            return { ...d.data(), id: d.id };
          })
        )
      );
    });

    const qp = query(collection(fire, "product"));
    onSnapshot(qp, (querySnapshot) => {
      setLoading((loads) => {
        return { ...loads, products: false };
      });
      dispatch(
        getProducts(
          querySnapshot.docs.map((d) => {
            return { ...d.data(), id: d.id };
          })
        )
      );
    });
  }, [dispatch]);
  return (
    <div>
      <AppSidebar />
      <div className="wrapper d-flex flex-column min-vh-100">
        <AppHeader />
        <div className="body flex-grow-1 bg-white">
          {loading.products && loading.categories ? (
            <div className="text-center my-5">
              <CSpinner color="primary" variant="grow" />
            </div>
          ) : (
            <AppContent />
          )}
        </div>
        <AppFooter />
      </div>
    </div>
  );
};

export default DefaultLayout;
