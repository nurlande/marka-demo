import {
  CCollapse,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CHeaderNav,
  CNavbarNav,
  CNavItem,
  CNavLink,
} from "@coreui/react";
import React from "react";
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { NavLink } from "react-router-dom";
import logo_main from "src/assets/logo_main.jpg";
import { accessoriesSub, shoesSub } from "src/constants/menuList";

export default function HeaderMenu({ categories }) {
  const { t, i18n } = useTranslation();

  const history = useHistory();

  return (
    <CHeaderNav className="d-none d-md-flex me-auto font-xs">
      <CNavItem>
        <CNavLink
          to="/home"
          component={NavLink}
          className="text-uppercase font-xl"
        >
          <img
            src={logo_main}
            alt="logo"
            className="img rounded-circle"
            style={{ width: "25px" }}
          />
          {t("CHOKOICHUM")}
        </CNavLink>
      </CNavItem>
      <CNavItem>
        <CNavLink
          to="/new-season"
          component={NavLink}
          className="text-uppercase"
        >
          {t("New Season")}
        </CNavLink>
      </CNavItem>
      <CNavItem>
        <CNavLink
          to="/promotions"
          component={NavLink}
          className="text-uppercase"
        >
          {t("Promotions")}
        </CNavLink>
      </CNavItem>
      {categories
        .filter((c) => c.name.toLowerCase() !== "accessories")
        .map((c, i) =>
          c.name.toLowerCase() === "shoes" ||
          c.name.toLowerCase() === "accessories" ? (
            <CNavItem key={i} className="mx-1">
              <CCollapse className="navbar-collapse" visible={true}>
                <CNavbarNav>
                  <CDropdown component="li" variant="nav-item">
                    <CDropdownToggle caret={false} className="text-uppercase">
                      {t(
                        i18n.language === "ky"
                          ? c.translate_kg || c.name
                          : c.translate_ru || c.name
                      )}
                    </CDropdownToggle>
                    {c.name.toLowerCase() === "shoes" ? (
                      <CDropdownMenu>
                        <CDropdownItem
                          className="n-link"
                          onClick={() => history.push("/products/shoes")}
                        >
                          {t("All")}
                        </CDropdownItem>
                        {shoesSub.map((sub) => (
                          <CDropdownItem
                            className="n-link"
                            onClick={() => history.push("/shoes/" + sub)}
                            key={sub}
                          >
                            {t(sub)}
                          </CDropdownItem>
                        ))}
                      </CDropdownMenu>
                    ) : (
                      <CDropdownMenu>
                        <CDropdownItem
                          className="n-link"
                          onClick={() => history.push("/products/accessories")}
                        >
                          {t("All")}
                        </CDropdownItem>
                        {accessoriesSub.map((sub) => (
                          <CDropdownItem
                            className="n-link"
                            onClick={() => history.push("/accessories/" + sub)}
                            key={sub}
                          >
                            {t(sub)}
                          </CDropdownItem>
                        ))}
                      </CDropdownMenu>
                    )}
                  </CDropdown>
                </CNavbarNav>
              </CCollapse>
            </CNavItem>
          ) : (
            <CNavItem key={i}>
              <CNavLink
                className="text-uppercase"
                to={"/products/" + c.name.toLowerCase()}
                component={NavLink}
              >
                {t(
                  i18n.language === "ky"
                    ? c.translate_kg || c.name
                    : c.translate_ru || c.name
                )}
              </CNavLink>
            </CNavItem>
          )
        )}
      <CNavItem>
        <CNavLink
          to="/most-sold"
          component={NavLink}
          className="text-uppercase"
        >
          {t("Most Sold")}
        </CNavLink>
      </CNavItem>
    </CHeaderNav>
  );
}
