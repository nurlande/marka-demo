import React from "react";
import {
  CAvatar,
  // CBadge,
  CDropdown,
  CDropdownDivider,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from "@coreui/react";
import {
  // cilBell,
  // cilCreditCard,
  // cilCommentSquare,
  // cilEnvelopeOpen,
  // cilFile,
  cilLockLocked,
  cilLockUnlocked,
  // cilSettings,
  // cilTask,
  // cilUser,
} from "@coreui/icons";
import CIcon from "@coreui/icons-react";

// import avatar8 from './../../assets/images/avatars/8.jpg'
import { setMode, setSidebarShow } from "src/redux/actions/settingsActions";
import { useDispatch } from "react-redux";
import { getAuth, signOut } from "firebase/auth";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

const AppHeaderDropdown = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { t } = useTranslation();

  const logOut = () => {
    const auth = getAuth();
    signOut(auth)
      .then(() => {
        localStorage.removeItem("markaUser");
        dispatch(setSidebarShow(false));
        dispatch(setMode("client"));
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <CDropdown variant="nav-item" alignment="end">
      <CDropdownToggle placement="bottom-end" className="py-0" caret={false}>
        <CAvatar
          src="https://cdn4.iconfinder.com/data/icons/e-commerce-181/512/477_profile__avatar__man_-512.png"
          size="md"
        />
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownHeader className="bg-light fw-semibold py-2">
          Профиль
        </CDropdownHeader>
        {getAuth().currentUser && (
          <CDropdownItem
            className="n-link"
            onClick={() => history.push("/personal-room")}
          >
            {t("My Room")}
          </CDropdownItem>
        )}
        <CDropdownDivider />
        {!getAuth().currentUser ? (
          <CDropdownItem onClick={() => history.push("/login")}>
            <CIcon
              icon={cilLockUnlocked}
              className="me-2"
              style={{ cursor: "pointer" }}
            />
            {t("Sign in")}
          </CDropdownItem>
        ) : (
          <CDropdownItem onClick={logOut}>
            <CIcon
              icon={cilLockLocked}
              className="me-2"
              style={{ cursor: "pointer" }}
            />
            {t("Sign out")}
          </CDropdownItem>
        )}
      </CDropdownMenu>
    </CDropdown>
  );
};

export default AppHeaderDropdown;
