import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CHeader,
  CHeaderBrand,
  CHeaderNav,
  CHeaderToggler,
  CNavItem,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
  CContainer,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { cifKg, cifRu, cilCart, cilHeart, cilMenu } from "@coreui/icons";

import { AppHeaderDropdown } from "./header/index";
import { setSidebarShow } from "src/redux/actions/settingsActions";
import { useHistory } from "react-router-dom";
import { getAuth } from "firebase/auth";
import HeaderMenu from "./HeaderMenu";
import logo_main from "src/assets/logo_main.jpg";
import { useTranslation } from "react-i18next";

const AppHeader = () => {
  const dispatch = useDispatch();

  const sidebarShow = useSelector((state) => state.settings.sidebarShow);
  // const cart = useSelector((state) => state.product.cart)
  const categories = useSelector((state) => state.product.categories);

  const { i18n } = useTranslation();
  const history = useHistory();
  const switchLang = (language) => {
    i18n.changeLanguage(language);
  };

  return (
    <CHeader position="sticky" className="mb-0">
      <CContainer>
        <CHeaderToggler
          className={"ps-1" + (!getAuth().currentUser ? " d-md-none" : "")}
          onClick={() => dispatch(setSidebarShow(!sidebarShow))}
        >
          <CIcon icon={cilMenu} size="lg" />
        </CHeaderToggler>
        <CHeaderBrand className="mx-auto d-md-none" to="/">
          <img
            src={logo_main}
            alt="logo"
            className="img rounded-circle"
            style={{ width: "25px" }}
          />
          CHOKOICHUM
        </CHeaderBrand>
        <HeaderMenu categories={categories} />
        <CHeaderNav>
          <CDropdown
            variant="nav-item"
            alignment="end"
            className="my-auto mx-1"
          >
            <CDropdownToggle>
              {i18n.language === "ru" ? (
                <>
                  <CIcon icon={cifRu} className="mr-1" />
                </>
              ) : (
                <>
                  <CIcon icon={cifKg} className="mr-1" />
                </>
              )}
            </CDropdownToggle>
            <CDropdownMenu placement="bottom-end">
              <CDropdownItem
                className="n-link"
                onClick={() => switchLang("ru")}
              >
                <CIcon icon={cifRu} className="mx-1" />
                Русский
              </CDropdownItem>
              <CDropdownItem
                className="n-link"
                onClick={() => switchLang("ky")}
              >
                <CIcon icon={cifKg} className="mx-1" />
                Кыргызча
              </CDropdownItem>
            </CDropdownMenu>
          </CDropdown>
          {getAuth().currentUser && (
            <CNavItem className="mx-2">
              <CIcon
                icon={cilHeart}
                style={{ height: "100%", cursor: "pointer" }}
                size="xl"
                onClick={() => history.push("/favourites")}
              />
            </CNavItem>
          )}
          <CNavItem>
            <CIcon
              icon={cilCart}
              style={{ height: "100%", cursor: "pointer" }}
              size="xl"
              onClick={() => history.push("/cart")}
            />
          </CNavItem>
        </CHeaderNav>
        <CHeaderNav className="ms-3">
          <AppHeaderDropdown />
        </CHeaderNav>
      </CContainer>
      {/* <CHeaderDivider /> */}
      {/* <CContainer fluid>
        <AppBreadcrumb />
      </CContainer> */}
    </CHeader>
  );
};

export default AppHeader;
