import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import {
  CNavGroup,
  CNavItem,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
} from "@coreui/react";
// import CIcon from '@coreui/icons-react'

import { AppSidebarNav } from "./AppSidebarNav";

// import { logoNegative } from 'src/assets/brand/logo-negative'
// import { sygnet } from 'src/assets/brand/sygnet'

import SimpleBar from "simplebar-react";
import "simplebar/dist/simplebar.min.css";
import navigation from "../_nav";
import { setSidebarShow } from "src/redux/actions/settingsActions";
// import CIcon from '@coreui/icons-react'
// import { cilList, cilStar } from '@coreui/icons'
import { useTranslation } from "react-i18next";
import { useHistory } from "react-router-dom";
import { accessoriesSub, shoesSub } from "src/constants/menuList";
import { t } from "i18next";

const AppSidebar = () => {
  const dispatch = useDispatch();

  const { i18n } = useTranslation();
  const history = useHistory();

  const unfoldable = useSelector((state) => state.settings.sidebarUnfoldable);
  const sidebarShow = useSelector((state) => state.settings.sidebarShow);
  const mode = useSelector((state) => state.settings.mode);
  const categories = useSelector((state) => state.product.categories);

  const [navMenu, setNavMenu] = useState(navigation);

  useEffect(() => {
    let catMenu = categories.map((c) => {
      if (c.name.toLowerCase() === "shoes") {
        return {
          component: CNavGroup,
          arrow: true,
          name:
            i18n.language === "ky"
              ? c.translate_kg || c.name
              : c.translate_ru || c.name,
          items: [
            {
              component: CNavItem,
              arrow: true,
              name: "All",
              to: "/products/shoes",
            },
            ...shoesSub.map((sub) => {
              return {
                component: CNavItem,
                arrow: true,
                name: t(sub),
                to: "/shoes/" + sub,
              };
            }),
          ],
        };
      } else if (c.name.toLowerCase() === "accessories") {
        return {
          component: CNavGroup,
          arrow: true,
          name:
            i18n.language === "ky"
              ? c.translate_kg || c.name
              : c.translate_ru || c.name,
          items: [
            {
              component: CNavItem,
              arrow: true,
              name: "All",
              to: "/products/accessories",
            },
            ...accessoriesSub.map((sub) => {
              return {
                component: CNavItem,
                arrow: true,
                name: t(sub),
                to: "/accessories/" + sub,
              };
            }),
          ],
        };
      }
      return {
        component: CNavItem,
        arrow: true,
        name:
          i18n.language === "ky"
            ? c.translate_kg || c.name
            : c.translate_ru || c.name,
        to: "/products/" + c.name.toLowerCase(),
      };
    });
    setNavMenu([...navigation, ...catMenu]);
  }, [categories, i18n]);

  return (
    <CSidebar
      position="fixed"
      unfoldable={unfoldable}
      visible={sidebarShow}
      onVisibleChange={(visible) => {
        dispatch(setSidebarShow(visible));
      }}
    >
      <CSidebarBrand
        className="d-none d-md-flex bg-light text-danger"
        onClick={() => history.push("/home")}
        style={{ cursor: "pointer" }}
      >
        CHOKOICHUM
        {/* <CIcon className="sidebar-brand-full" icon={logoNegative} height={35} />
        <CIcon className="sidebar-brand-narrow" icon={sygnet} height={35} /> */}
      </CSidebarBrand>
      <CSidebarNav className="bg-light">
        <SimpleBar>
          <AppSidebarNav
            items={
              mode === "client"
                ? navMenu.filter((n) => n.mode !== "admin")
                : navMenu
            }
          />
        </SimpleBar>
      </CSidebarNav>
      {/* <CSidebarToggler
        className="d-none d-lg-flex"
        onClick={() => dispatch(setSidebarUnfold(!unfoldable))}
      /> */}
    </CSidebar>
  );
};

export default React.memo(AppSidebar);
