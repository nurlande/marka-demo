import React from 'react'
import { NavLink, useLocation } from 'react-router-dom'
import PropTypes from 'prop-types'

import { CBadge } from '@coreui/react'
import { useTranslation } from 'react-i18next'
import { useDispatch } from 'react-redux'
import { setSidebarShow } from '../redux/actions/settingsActions'
import arrowmenu from 'src/assets/arrowmenu.svg'

export const AppSidebarNav = ({ items }) => {
  const location = useLocation()
  const {t} = useTranslation()

  const dispatch = useDispatch()
  
  const navLink = (name, icon, badge, arrow) => {
    return (
      <>
      {/* <div className='d-flex justify-content-between bg-light text-dark'> */}
        {icon && icon}
        {name && <span className='text-dark'>{t(name)}</span>}
        {arrow && <img alt="menuarrow" src={arrowmenu} width="10" style={{position: "absolute", right: 10}}/>}
        {badge && (
          <CBadge color={badge.color} className="ms-auto">
            {badge.text}
          </CBadge>
        )}
      {/* </div> */}
      </>
    )
  }

  const navItem = (item, index) => {
    const { component, name, badge, icon, arrow, ...rest } = item
    const Component = component
    return (
      <Component
        {...(rest.to &&
          !rest.items && {
            component: NavLink,
            activeClassName: 'active bg-white text-primary',
          })}
        key={index}
        {...rest}
        className="bg-light text-dark"
        onClick={() => dispatch(setSidebarShow(false))}
      >
        {navLink(name, icon, badge, arrow)}
      </Component>
    )
  }
  const navGroup = (item, index) => {
    const { component, name, icon, to, badge, arrow, ...rest } = item
    const Component = component
    return (
      <Component
        idx={String(index)}
        key={index}
        toggler={navLink(name, icon, badge, arrow)}
        visible={location.pathname.startsWith(to)}
        {...rest}
        className="bg-light text-dark"
      >
        {item.items?.map((item, index) =>
          item.items ? navGroup(item, index) : navItem(item, index),
        )}
      </Component>
    )
  }

  return (
    <React.Fragment>
      {items &&
        items.map((item, index) => (item.items ? navGroup(item, index) : navItem(item, index)))}
    </React.Fragment>
  )
}

AppSidebarNav.propTypes = {
  items: PropTypes.arrayOf(PropTypes.any).isRequired,
}
