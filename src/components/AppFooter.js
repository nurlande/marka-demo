import React from 'react'
import { CFooter } from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cibInstagram, cibWhatsapp, cilPhone } from '@coreui/icons'

const AppFooter = () => {
  return (
    <CFooter>
      <div>
        <div className='pl-4' style={{cursor: "pointer"}} onClick={() => window.open("https://instagram.com/chokoichum", "_blank")}>
          <CIcon icon={cibInstagram}/> CHOKOICHUM
        </div>
        <div className='pl-4' style={{cursor: "pointer"}} onClick={() => window.open("https://wa.me/+996755959526", "_blank")}>
          <CIcon icon={cibWhatsapp}/> +996 755 959 526
        </div>
        <div className='pl-4' style={{cursor: "pointer"}} onClick={() => window.open("tel:+996704305040", "_blank")}>
          <CIcon icon={cilPhone}/> +996 704 305 040
        </div>
      </div>
      <div className="ms-auto">
        <span className="me-1">Developed by</span>
        <a href="https://nurlan-dev.web.app" target="_blank" rel="noopener noreferrer">
          N-Dev
        </a>
      </div>
    </CFooter>
  )
}

export default React.memo(AppFooter)
