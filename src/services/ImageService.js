import { collection, getDocs, query, where } from "firebase/firestore";
import { fire } from "src/configs/fire";

class ProductService {
  getAll = (params) => {
    return new Promise((resolve, reject) => {
      const qp = query(
        collection(fire, "product"),
        where("category", "==", params.category)
      );
      getDocs(qp).then((querySnapshot) => {
        resolve(
          querySnapshot.docs.map((d) => {
            return { ...d.data(), id: d.id };
          })
        );
      });
    });
  };
}

export default new ProductService();
