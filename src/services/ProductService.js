import {
  collection,
  addDoc,
  setDoc,
  getDocs,
  query,
  where,
  doc,
  deleteDoc,
  getDoc,
} from "firebase/firestore";
import { fire } from "src/configs/fire";

class ProductService {
  collectionRef = collection(fire, "product");

  getDocRef = (id) => {
    return doc(fire, "product", id);
  };

  getAll = (params) => {
    return new Promise((resolve, reject) => {
      const conditions = params
        ? [where("category", "==", params.category)]
        : [];
      const qp = query(this.collectionRef, ...conditions);
      getDocs(qp).then((querySnapshot) => {
        resolve(
          querySnapshot.docs.map((d) => {
            return { ...d.data(), id: d.id };
          })
        );
      });
    });
  };

  getById = (id) => {
    return getDoc(this.getDocRef(id));
  };

  create = (product) => {
    return addDoc(this.collectionRef, product);
  };

  update = (id, product) => {
    return setDoc(this.getDocRef(id), product);
  };

  delete = (id) => {
    return deleteDoc(this.getDocRef(id));
  };

  updateComments = (id, newComments) => {
    return setDoc(
      this.getDocRef(id),
      { comments: newComments },
      { merge: true }
    );
  };
}

export default new ProductService();
