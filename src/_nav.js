// import React from 'react'
// import CIcon from '@coreui/icons-react'
// import {
//   cilSpeedometer, cilStar,
// } from '@coreui/icons'
import {
  // CNavGroup,
  CNavItem,
  CNavTitle,
} from "@coreui/react";

const _nav = [
  {
    component: CNavItem,
    name: "Home",
    to: "/",
    // icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon text-dark" />
  },
  {
    component: CNavTitle,
    name: "Admin",
    mode: "admin",
  },
  // {
  //   component: CNavItem,
  //   name: 'Categories',
  //   mode: 'admin',
  //   to: '/admin/categories',
  //   icon: <CIcon icon={cilStar} customClassName="nav-icon text-dark" />
  // },
  {
    component: CNavItem,
    name: "Products",
    to: "/admin/products",
    // icon: <CIcon icon={cilStar} customClassName="nav-icon text-dark" />,
    mode: "admin",
  },
  {
    component: CNavItem,
    name: "Orders",
    to: "/admin/orders",
    // icon: <CIcon icon={cilStar} customClassName="nav-icon text-dark" />,
    mode: "admin",
  },
  {
    component: CNavItem,
    name: "Marks",
    to: "/admin/marks",
    // icon: <CIcon icon={cilStar} customClassName="nav-icon text-dark" />,
    mode: "admin",
  },
  {
    component: CNavItem,
    name: "Colors",
    to: "/admin/colors",
    // icon: <CIcon icon={cilStar} customClassName="nav-icon text-dark" />,
    mode: "admin",
  },
  {
    component: CNavItem,
    name: "Problems",
    to: "/admin/problems",
    // icon: <CIcon icon={cilStar} customClassName="nav-icon text-dark" />,
    mode: "admin",
  },
  {
    component: CNavTitle,
    name: "Products",
  },
  {
    component: CNavItem,
    arrow: true,
    name: "New Season",
    to: "/new-season",
    // icon: <CIcon icon={cilStar} customClassName="nav-icon text-dark" />,
  },
  {
    component: CNavItem,
    arrow: true,
    name: "Promotions",
    to: "/promotions",
    // icon: <CIcon icon={cilStar} customClassName="nav-icon text-dark" />,
  },
];

export default _nav;
